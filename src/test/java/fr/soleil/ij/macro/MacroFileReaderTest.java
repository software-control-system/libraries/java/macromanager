package fr.soleil.ij.macro;

import org.junit.Test;

import ij.IJ;

public class MacroFileReaderTest {

    public MacroFileReaderTest() {
        super();
    }

    @Test
    public void fakeTest() {
        assert (true);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            IJ.runMacroFile("D:\\test\\FileReaderMacro.txt", args[0]);
        } else {
            IJ.runMacroFile("D:\\test\\FileReaderMacro.txt");
        }
    }

}
