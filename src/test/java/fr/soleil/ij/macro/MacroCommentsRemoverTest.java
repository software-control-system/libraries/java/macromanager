package fr.soleil.ij.macro;

import java.io.File;

import org.junit.Test;

import fr.soleil.ij.macro.header.MacroCommentsRemover;

public class MacroCommentsRemoverTest {

    public MacroCommentsRemoverTest() {
        super();
    }

    @Test
    public void testRemoveComments() {
        String toAnalyze = "toto 0 a 52236:47 test\ntu/*tu/*I'm with stupid\nme too 7512*********/***//a234.8-6b/**//////";
        String result = MacroCommentsRemover.removeComments(toAnalyze);
        String expectedResult = "toto 0 a 52236:47 test\ntu ***//a234.8-6b /////";
        assert(expectedResult.equals(result));
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        String toAnalyze = "toto 0 a 52236:47 test\ntu/*tu/*I'm with stupid\nme too 7512*********/***//a234.8-6b/**//////";
        System.out.println("Test 1 :");
        System.out.println("-----Original String-----");
        System.out.println(toAnalyze);
        System.out.println("---------Result----------");
        System.out.println(MacroCommentsRemover.removeComments(toAnalyze));
        for (int i = 0; i < args.length; i++) {
            System.out.println("=========================");
            System.out.println("Test " + (i + 2) + " :");
            System.out.println("---------Result----------");
            System.out.println(MacroCommentsRemover
                    .extractMacroWithoutComments(new File(args[i])));
        }
    }

}
