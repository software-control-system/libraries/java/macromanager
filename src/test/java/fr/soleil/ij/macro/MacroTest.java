package fr.soleil.ij.macro;

import org.junit.Test;

import fr.soleil.ij.macro.element.MacroElementType;
import fr.soleil.ij.macro.interactable.MacroInteractableList;
import fr.soleil.ij.macro.interactable.MacroInteractableSingle;

public class MacroTest {

    public MacroTest() {
        super();
    }

    @Test
    public void testMacroLoading() {
        String toTest = "// HEADER_START\n// parameter_count:2\n// parameter_0_cardinality : 1\n// parameter_0_type : intscalar\n    // parameter_1_cardinality : n\n  //      parameter_1_type:intspectrum\n// result_count:3\n  //                                  result_0_cardinality: n\n   //                                  result_0_type               : floatscalar\n//result_1_cardinality:1\n//result_1_type:floatspectrum\n//result_2_cardinality:n\n//result_2_type:stringscalar\n// HEADER_END";
        try {
            Macro macro = new Macro(toTest, false);
            if (macro.getParameters() == null || macro.getParameters().length != 2) {
                System.out.println("parameter length ko");
                assert (false);
            }
            if (macro.getParameters()[0].getExpectedElementType() != MacroElementType.SCALAR_INT) {
                System.out.println("parameter 0 type ko");
                assert (false);
            }
            if (!(macro.getParameters()[0] instanceof MacroInteractableSingle<?>)) {
                System.out.println("parameter 0 cardinality ko");
                assert (false);
            }
            if (macro.getParameters()[1].getExpectedElementType() != MacroElementType.SPECTRUM_INT) {
                System.out.println("parameter 1 type ko");
                assert (false);
            }
            if (!(macro.getParameters()[1] instanceof MacroInteractableList<?>)) {
                System.out.println("parameter 1 cardinality ko");
                assert (false);
            }
            if (macro.getResults() == null || macro.getResults().length != 3) {
                System.out.println("result length ko");
                assert (false);
            }
            if (macro.getResults()[0].getExpectedElementType() != MacroElementType.SCALAR_FLOAT) {
                System.out.println("result 0 type ko");
                assert (false);
            }
            if (!(macro.getResults()[0] instanceof MacroInteractableList<?>)) {
                System.out.println("result 0 cardinality ko");
                assert (false);
            }
            if (macro.getResults()[1].getExpectedElementType() != MacroElementType.SPECTRUM_FLOAT) {
                System.out.println("result 1 type ko");
                assert (false);
            }
            if (!(macro.getResults()[1] instanceof MacroInteractableSingle<?>)) {
                System.out.println("result 1 cardinality ko");
                assert (false);
            }
            if (macro.getResults()[2].getExpectedElementType() != MacroElementType.SCALAR_STRING) {
                System.out.println("result 2 type ko");
                assert (false);
            }
            if (!(macro.getResults()[2] instanceof MacroInteractableList<?>)) {
                System.out.println("result 2 cardinality ko");
                assert (false);
            }
        } catch (Exception e) {
            System.out.println("exception ko");
            e.printStackTrace();
            assert (false);
        }
        assert (true);
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        for (int i = 0; i < args.length; i++) {
            System.out.println("==================================");
            System.out.println("Trying to analyse macro: " + args[i]);
            Macro macro = new Macro(args[i], true);
            if ((macro.getDescription() != null) && !macro.getDescription().trim().isEmpty()) {
                System.out.println("..................................");
                System.out.println("description: " + macro.getDescription());
            }
            System.out.println("..................................");
            System.out.println("checking parameters:");
            if (macro.getParameters() == null || macro.getParameters().length == 0) {
                System.out.println("no parameter");
            } else {
                for (int j = 0; j < macro.getParameters().length; j++) {
                    System.out.println(" - parameter " + j + ":");
                    if ((macro.getParameters()[j].getDescription() != null)
                            && !macro.getParameters()[j].getDescription().isEmpty()) {
                        System.out.println("\tdescription: " + macro.getParameters()[j].getDescription());
                    }
                    System.out.println("\t" + macro.getParameters()[j].getClass().getName());
                    System.out.println("\t" + macro.getParameters()[j].getExpectedElementType().toString());
                }
            }
            System.out.println("..................................");
            System.out.println("checking results:");
            if (macro.getResults() == null || macro.getResults().length == 0) {
                System.out.println("no result");
            } else {
                for (int j = 0; j < macro.getResults().length; j++) {
                    System.out.println(" - result " + j + ":");
                    if ((macro.getResults()[j].getDescription() != null)
                            && !macro.getResults()[j].getDescription().isEmpty()) {
                        System.out.println("\tdescription: " + macro.getResults()[j].getDescription());
                    }
                    System.out.println("\t" + macro.getResults()[j].getClass().getName());
                    System.out.println("\t" + macro.getResults()[j].getExpectedElementType().toString());
                }
            }
        }
    }

}
