package fr.soleil.ij.macro;

import java.util.Collection;

import org.junit.Test;

import fr.soleil.ij.macro.exception.MacroHeaderException;
import fr.soleil.ij.macro.header.MacroHeaderExtractor;

public class MacroHeaderExtractorTest {

    public MacroHeaderExtractorTest() {
        super();
    }

    @Test
    public void testLoadKeys() {
        String toTest = "// HEADER_START\n// val1:val2\n// val3 : val\n  //                                  test: truc\n  //  bidule :chose\n// HEADER_END";
        MacroHeaderExtractor extractor = new MacroHeaderExtractor();
        try {
            extractor.extractMacroHead(toTest);
        } catch (MacroHeaderException e) {
            e.printStackTrace();
            assert (false);
        }
        if (!"val2".equals(extractor.getValueFor("val1"))) {
            assert (false);
        }
        if (!"val".equals(extractor.getValueFor("val3"))) {
            assert (false);
        }
        if (!"truc".equals(extractor.getValueFor("test"))) {
            assert (false);
        }
        if (!"chose".equals(extractor.getValueFor("bidule"))) {
            assert (false);
        }
        assert (true);
    }

    public static void main(String[] args) throws MacroHeaderException {
        MacroHeaderExtractor extractor = new MacroHeaderExtractor();
        for (int i = 0; i < args.length; i++) {
            System.out.println("=========================");
            System.out.println("Test " + (i + 1) + " : " + args[i]);
            System.out.println("-------Extracted ?-------");
            if (extractor.extractMacroFileHead(args[i])) {
                System.out.println(true);
                System.out.println("-----Keys --> Values-----");
                Collection<String> keys = extractor.getKeys();
                for (String key : keys) {
                    System.out.print(key);
                    System.out.print(" --> ");
                    System.out.println(extractor.getValueFor(key));
                }
            } else {
                System.out.println(false);
            }
        }
    }

}
