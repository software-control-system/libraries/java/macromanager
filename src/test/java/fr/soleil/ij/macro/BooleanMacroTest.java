package fr.soleil.ij.macro;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import fr.soleil.ij.macro.element.BooleanScalarMacroElement;
import fr.soleil.ij.macro.element.BooleanSpectrumMacroElement;
import fr.soleil.ij.macro.element.MacroElementType;
import fr.soleil.ij.macro.exception.MacroException;
import fr.soleil.ij.macro.interactable.MacroInteractable;
import fr.soleil.ij.macro.interactable.MacroInteractableSingle;
import fr.soleil.lib.project.file.FileUtils;

public class BooleanMacroTest {

    private static final String MACRO_PATH = "example/BooleanMacroTest.txt";

    public BooleanMacroTest() {
        super();
    }

    private boolean generateBoolean() {
        return Math.random() < 0.5;
    }

    @Test
    public void testBooleanMacro() throws IOException, MacroException {
        try {
            InputStream inputStream = getClass().getResourceAsStream(MACRO_PATH);
            Assert.assertNotNull("Failed to find " + MACRO_PATH, inputStream);
            InputStreamReader reader = new InputStreamReader(inputStream);
            String macroText = FileUtils.readTextStream(reader);
            Assert.assertNotNull("Failed to read " + MACRO_PATH, macroText);
            Macro macro = new Macro(macroText);
            Assert.assertNotNull("Macro parameters should not be null!", macro.getParameters());
            Assert.assertEquals("Macro parameters are of length " + macro.getParameters().length + " instead of 2",
                    macro.getParameters().length, 2);
            MacroInteractable<?> tmp = macro.getParameters()[0];
            Assert.assertTrue("1st parameter is not of expected cardinality",
                    tmp instanceof MacroInteractableSingle<?>);
            Assert.assertTrue("1st parameter is not of expected data type",
                    tmp.getExpectedElementType() == MacroElementType.SPECTRUM_BOOLEAN);
            MacroInteractableSingle<?> single = (MacroInteractableSingle<?>) tmp;
            BooleanSpectrumMacroElement firstParam = new BooleanSpectrumMacroElement();
            firstParam.setName("input spectrum");
            boolean[] firstParamValue = new boolean[new Random().nextInt(10) + 1];
            for (int i = 0; i < firstParamValue.length; i++) {
                firstParamValue[i] = generateBoolean();
            }
            firstParam.setValue(firstParamValue);
            @SuppressWarnings("unchecked")
            MacroInteractableSingle<BooleanSpectrumMacroElement> spectrumParam = (MacroInteractableSingle<BooleanSpectrumMacroElement>) single;
            spectrumParam.setValue(firstParam);
            tmp = macro.getParameters()[1];
            Assert.assertTrue("2nd parameter is not of expected cardinality",
                    tmp instanceof MacroInteractableSingle<?>);
            Assert.assertTrue("2nd parameter is not of expected data type",
                    tmp.getExpectedElementType() == MacroElementType.SCALAR_BOOLEAN);
            single = (MacroInteractableSingle<?>) tmp;
            BooleanScalarMacroElement secondParam = new BooleanScalarMacroElement();
            secondParam.setName("input scalar");
            Boolean secondParamValue = Boolean.valueOf(generateBoolean());
            secondParam.setValue(secondParamValue);
            @SuppressWarnings("unchecked")
            MacroInteractableSingle<BooleanScalarMacroElement> scalarParam = (MacroInteractableSingle<BooleanScalarMacroElement>) single;
            scalarParam.setValue(secondParam);
            macro.run();
            Assert.assertNotNull("Macro results should not be null!", macro.getResults());
            Assert.assertEquals("Macro results are of length " + macro.getResults().length + " instead of 2",
                    macro.getResults().length, 2);
            tmp = macro.getResults()[0];
            Assert.assertTrue("1st result is not of expected cardinality", tmp instanceof MacroInteractableSingle<?>);
            Assert.assertTrue("1st result is not of expected data type",
                    tmp.getExpectedElementType() == MacroElementType.SCALAR_BOOLEAN);
            single = (MacroInteractableSingle<?>) tmp;
            Assert.assertTrue("1st result value is not a boolean scalar",
                    single.getValue() instanceof BooleanScalarMacroElement);
            BooleanScalarMacroElement firstResult = (BooleanScalarMacroElement) single.getValue();
            Boolean firstResultValue = firstResult.getValue();
            Assert.assertNotNull("1st result value can't be null", firstResultValue);
            Assert.assertEquals("1st result value should be the opposite of 2nd parameter value: " + firstResultValue
                    + " VS " + secondParamValue, firstResultValue, Boolean.valueOf(!secondParamValue.booleanValue()));
            tmp = macro.getResults()[1];
            Assert.assertTrue("2nd result is not of expected cardinality", tmp instanceof MacroInteractableSingle<?>);
            Assert.assertTrue("2nd result is not of expected data type",
                    tmp.getExpectedElementType() == MacroElementType.SPECTRUM_BOOLEAN);
            single = (MacroInteractableSingle<?>) tmp;
            Assert.assertTrue("2nd result value is not a boolean spectrum",
                    single.getValue() instanceof BooleanSpectrumMacroElement);
            BooleanSpectrumMacroElement secondResult = (BooleanSpectrumMacroElement) single.getValue();
            boolean[] secondResultValue = secondResult.getValue();
            Assert.assertNotNull("2nd result value should not be null", secondResult.getValue());
            Assert.assertEquals("1st param and second result values are not of same length", firstParamValue.length,
                    secondResultValue.length);
            for (int i = 0; i < firstParamValue.length; i++) {
                Assert.assertFalse("2nd result value is not the opposite of 1st parameter value",
                        firstParamValue[i] == secondResultValue[i]);
            }
        } catch (MacroException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
