package fr.soleil.ij.macro;

import fr.soleil.ij.macro.exception.MacroCompatibilityException;

public abstract class AbstractRunnableMacro extends AbstractMacro {

    public AbstractRunnableMacro(String macro) {
        super(macro);
    }

    /**
     * Runs the macro, and finishes building the results (sets the missing values)
     * 
     * @throws MacroCompatibilityException If a problem occurred during macro execution
     */
    public abstract void run() throws MacroCompatibilityException;

    @Override
    public AbstractRunnableMacro clone() {
        return (AbstractRunnableMacro) super.clone();
    }

}
