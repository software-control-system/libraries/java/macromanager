package fr.soleil.ij.macro.util;

public class MacroUtils {

    public static final Double TRUE_NUMBER = Double.valueOf(1);
    public static final Double FALSE_NUMBER = Double.valueOf(0);
    public static final String TRUE_STRING = "true";
    public static final String FALSE_STRING = "false";
    public static final String TRUE_NUMBER_STRING = "1";
    public static final String FALSE_NUMBER_STRING = "0";

    public static final String EMPTY_STRING = "";
    public static final String KEY_SEPARATOR = "_";
    public static final String SPACE = " ";
    public static final String NULL = "null";
    public static final String NEW_LINE = "\n";
    public static final String NEW_LINE_REPLACEMENT = "#NewLine#";
    public static final String SEMICOLON = ";";
    public static final String SEMICOLON_REPLACEMENT = "#semicolon#";
    public static final String SLASH = "/";
    public static final String SLASH_REPLACEMENT = "#slash#";
    public static final String DOUBLE_SLASH = "//";
    public static final String DOUBLE_QUOT = "\"";

    protected static boolean toBooleanNoCheck(Double value) {
        return (value.byteValue() != 0);
    }

    public static Double toDouble(boolean value) {
        return value ? TRUE_NUMBER : FALSE_NUMBER;
    }

    public static boolean toBoolean(Double value) {
        boolean result;
        if (value == null) {
            result = false;
        } else {
            result = toBooleanNoCheck(value);
        }
        return result;
    }

    public static boolean toBooleanFromNbObject(Object value) {
        boolean result;
        if (value instanceof Double) {
            result = toBooleanNoCheck((Double) value);
        } else {
            result = false;
        }
        return result;
    }

    public static boolean toBooleanFromString(String value) {
        boolean result;
        if (TRUE_NUMBER_STRING.equals(value) || FALSE_NUMBER_STRING.equals(value)) {
            result = Boolean.valueOf(TRUE_NUMBER_STRING.equals(value));
        } else {
            result = TRUE_STRING.equalsIgnoreCase(value);
        }
        return result;
    }
}
