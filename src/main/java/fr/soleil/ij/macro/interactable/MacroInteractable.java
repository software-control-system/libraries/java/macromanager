package fr.soleil.ij.macro.interactable;

import fr.soleil.ij.macro.element.IMacroElement;
import fr.soleil.ij.macro.element.MacroElementType;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;

/**
 * Class used to interact with a macro. Can be used to represent a macro parameter or result. A
 * {@link MacroInteractable} is supposed to be compatible with a particular {@link MacroElementType} .
 * 
 * @param <T> The kind of value to expect for this {@link MacroInteractable}
 * @author GIRARDOT
 */
public abstract class MacroInteractable<T> implements Cloneable {

    protected String displayShortName;
    protected String displayName;
    protected String description;
    protected T value;
    protected MacroElementType expectedElementType;

    /**
     * Constructor
     * 
     * @param description a description for this {@link MacroInteractable}
     * @param displayShortName the display short name for this {@link MacroInteractable}
     * @param displayName the display name for this {@link MacroInteractable}
     * @param expectedElementType the {@link MacroElementType} this {@link MacroInteractable} is
     *            supposed to be compatible with.
     * @throws MacroCompatibilityException if <code>expectedElementType</code> is <code>null</code>
     */
    public MacroInteractable(String description, String displayShortName, String displayName,
            MacroElementType expectedElementType) throws MacroCompatibilityException {
        super();
        this.description = description;
        this.displayShortName = displayShortName;
        this.displayName = displayName;
        this.value = null;
        if (expectedElementType == null) {
            throw new MacroCompatibilityException("expectedElementType can't be null");
        }
        this.expectedElementType = expectedElementType;
    }

    /**
     * Returns the description of this {@link MacroInteractable}
     * 
     * @return A {@link String}
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the display short name of this {@link MacroInteractable}
     * 
     * @return A {@link String}
     */
    public String getDisplayShortName() {
        return displayShortName;
    }

    /**
     * Returns the display name of this {@link MacroInteractable}
     * 
     * @return a {@link String}
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Returns the value associated with this {@link MacroInteractable}. Can be <code>null</code> if
     * not yet set.
     * 
     * @return the value associated with this {@link MacroInteractable}
     * @see #setValue(Object)
     */
    public T getValue() {
        return value;
    }

    /**
     * Sets the value of this the value associated with this {@link MacroInteractable}.
     * 
     * @param value the value to set
     * @throws MacroCompatibilityException if the value is not compatible with the declared {@link MacroElementType}
     */
    public void setValue(T value) throws MacroCompatibilityException {
        checkValue(value);
        this.value = value;
    }

    /**
     * Returns the {@link MacroElementType} this {@link MacroInteractable} is supposed to be
     * compatible with. Will never be <code>null</code>, due to constructor.
     * 
     * @return a {@link MacroElementType}
     */
    public MacroElementType getExpectedElementType() {
        return expectedElementType;
    }

    protected abstract void checkValue(T value) throws MacroCompatibilityException;

    /**
     * Appends the {@link String} representation of the {@link MacroInteractable} in a {@link StringBuilder}
     * 
     * @param buffer the {@link StringBuilder} to which to append the {@link String} representation.
     *            Can be <code>null</code>
     * @return the {@link StringBuilder} in parameter to which the {@link String} representation was
     *         added. If <code>null</code> a new {@link StringBuilder} containing the {@link String} representation is
     *         returned.
     */
    public abstract StringBuilder toWritableStringBuilder(StringBuilder buffer);

    protected boolean hasValidElementType(IMacroElement<?> element) {
        if (element == null) {
            return true;
        } else {
            return getExpectedElementType() == element.getElementType();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public MacroInteractable<T> clone() {
        try {
            return (MacroInteractable<T>) super.clone();
        } catch (CloneNotSupportedException e) {
            // should never happen
            return this;
        }
    }

    /**
     * Method used to force adapters cleaning
     */
    public abstract void unregister();

}
