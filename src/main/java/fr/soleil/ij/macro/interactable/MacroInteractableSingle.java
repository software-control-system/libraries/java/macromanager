package fr.soleil.ij.macro.interactable;

import fr.soleil.ij.macro.element.DefaultMacroElement;
import fr.soleil.ij.macro.element.IMacroElement;
import fr.soleil.ij.macro.element.MacroElementType;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;

/**
 * MacroInteractableSingle is a {@link MacroInteractable} which value is supposed to be an {@link IMacroElement}.
 * 
 * @param <T> the kind of {@link IMacroElement} to expect
 * @author GIRARDOT
 */
public class MacroInteractableSingle<T extends DefaultMacroElement<?>> extends MacroInteractable<T> {

    /**
     * Constructor
     * 
     * @param description a description for this {@link MacroInteractable}
     * @param displayShortName the display short name for this {@link MacroInteractable}
     * @param displayName the display name for this {@link MacroInteractable}
     * @param expectedElementType the {@link MacroElementType} the {@link IMacroElement} is supposed
     *            to be compatible with.
     * @throws MacroCompatibilityException if <code>expectedElementType</code> is <code>null</code>
     */
    public MacroInteractableSingle(String description, String displayShortName, String displayName,
            MacroElementType expectedElementType) throws MacroCompatibilityException {
        super(description, displayShortName, displayName, expectedElementType);
    }

    @Override
    public StringBuilder toWritableStringBuilder(StringBuilder buffer) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        if (value != null) {
            return value.toWritableStringBuilder(buffer);
        }
        return buffer;
    }

    @Override
    protected void checkValue(T value) throws MacroCompatibilityException {
        if (!hasValidElementType(value)) {
            throw new MacroCompatibilityException(
                    "incompatible type: expected " + getExpectedElementType() + " and got " + value.getElementType());
        }
    }

    @Override
    public void unregister() {
        if (getValue() != null) {
            getValue().unregisterContent();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public MacroInteractableSingle<T> clone() {
        MacroInteractableSingle<T> result = (MacroInteractableSingle<T>) super.clone();
        if (value == null) {
            result.value = null;
        } else {
            result.value = (T) value.clone();
        }
        return result;
    }

}
