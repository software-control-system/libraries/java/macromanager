package fr.soleil.ij.macro.interactable;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.ij.macro.element.DefaultMacroElement;
import fr.soleil.ij.macro.element.IMacroElement;
import fr.soleil.ij.macro.element.MacroElementType;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * A {@link MacroInteractableList} is a {@link MacroInteractable} which value is supposed to be an {@link ArrayList} of
 * {@link IMacroElement}. This is useful, for example, to represent a macro parameter that is itself a list of elements.
 * 
 * @param <T> the kind of {@link IMacroElement} to expect in {@link ArrayList}.
 * @author GIRARDOT
 */
public class MacroInteractableList<T extends DefaultMacroElement<?>> extends MacroInteractable<List<T>> {

    /**
     * Constructor
     * 
     * @param description a description for this {@link MacroInteractable}
     * @param displayShortName the display short name for this {@link MacroInteractable}
     * @param displayName the display name for this {@link MacroInteractable}
     * @param expectedElementType the {@link MacroElementType} the {@link IMacroElement}s in {@link ArrayList} are
     *            supposed to be compatible with.
     * @throws MacroCompatibilityException if <code>expectedElementType</code> is <code>null</code>
     */
    public MacroInteractableList(String description, String displayShortName, String displayName,
            MacroElementType expectedElementType) throws MacroCompatibilityException {
        super(description, displayShortName, displayName, expectedElementType);
    }

    @Override
    public StringBuilder toWritableStringBuilder(StringBuilder buffer) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        if (value != null) {
            for (int i = 0; i < value.size(); i++) {
                if (value.get(i) == null) {
                    buffer.append(MacroUtils.NULL);
                } else {
                    value.get(i).toWritableStringBuilder(buffer);
                }
                if (i < value.size() - 1) {
                    buffer.append('\n');
                }
            }
        }
        return buffer;
    }

    /**
     * Returns a copy of the list associated with this {@link MacroInteractableList}
     * 
     * @return an {@link ArrayList}
     */
    @Override
    public List<T> getValue() {
        List<T> result;
        if (value == null) {
            result = null;
        } else {
            result = new ArrayList<T>(value);
        }
        return result;
    }

    @Override
    protected void checkValue(List<T> value) throws MacroCompatibilityException {
        if (value != null) {
            for (int i = 0; i < value.size(); i++) {
                if (!hasValidElementType(value.get(i))) {
                    throw new MacroCompatibilityException("incompatible type: expected " + getExpectedElementType()
                            + " and got " + value.get(i).getElementType());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public MacroInteractableList<T> clone() {
        MacroInteractableList<T> result = (MacroInteractableList<T>) super.clone();
        if (value == null) {
            result.value = null;
        } else {
            result.value = new ArrayList<T>(value.size());
            for (T subValue : value) {
                if (subValue == null) {
                    result.value.add(null);
                } else {
                    result.value.add((T) subValue.clone());
                }
            }
        }
        return result;
    }

    @Override
    public void unregister() {
        if (getValue() != null) {
            for (int i = 0; i < getValue().size(); i++) {
                DefaultMacroElement<?> element = getValue().get(i);
                if (element != null) {
                    element.unregisterContent();
                }
            }
        }
    }

}
