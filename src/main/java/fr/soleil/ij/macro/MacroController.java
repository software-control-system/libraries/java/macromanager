package fr.soleil.ij.macro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import fr.soleil.ij.macro.util.MacroUtils;
import ij.IJ;

/**
 * This class is used by {@link Macro} to interact with macro files. It offers the possibility to read files, and to
 * transmit the parameters to ImageJ to run the macro.
 * 
 * @author GIRARDOT
 */
public class MacroController {

    public final static String MACRO_INTERACTABLE_SEPARATOR = "#####";

    /**
     * Reads a File and returns its content as a {@link String}
     * 
     * @param fileName the path of the File to read
     * @return a String, which represents the file content
     */
    public static String readSelectedFile(String fileName) {
        String result;
        try {
            File file = new File(fileName);
            StringBuilder sb = new StringBuilder(5000);
            if (file != null && file.isFile()) {
                BufferedReader r = new BufferedReader(new FileReader(file));
                if (r != null) {
                    String s = r.readLine();
                    while (s != null) {
                        sb.append(s).append('\n');
                        s = r.readLine();
                    }
                    if (sb.length() > 0) {
                        // remove last '\n'
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    r.close();
                }
            }
            result = sb.toString();
        } catch (Exception e) {
            result = MacroUtils.EMPTY_STRING;
        }
        return result;
    }

    /**
     * Asks ImageJ to run a macro code. Returns the macro result, if available, or <code>null</code> in case of error.
     * 
     * @param macro The macro code to run
     * @param arg The argument to give to the macro
     * @return The macro result, if available, or <code>null</code> in case of error.
     */
    public static String runMacro(String macro, String arg) {
        String result;
        if ((macro != null) && !macro.trim().isEmpty()) {
            result = IJ.runMacro(macro, arg);
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Asks ImageJ to run a macro file. Returns the macro result, if available, or <code>null</code> in case of error.
     * 
     * @param macro The path of the macro to run
     * @param arg The argument to give to the macro
     * @return The macro result, if available, or <code>null</code> in case of error.
     */
    public static String runMacroFile(String path, String arg) {
        String result;
        if ((path != null) && !path.trim().isEmpty()) {
            File file = new File(path);
            if (file.exists()) {
                result = IJ.runMacro(readSelectedFile(path), arg);
            } else {
                result = null;
            }
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Finds the macro files associated to the directory in parameter
     * 
     * @param dir directory path
     * @return The macro files paths, represented as a {@link String}[]
     */
    public static String[] getMacroFiles(String dir) {
        File listOfMacros = new File(dir);
        String[] listOfFiles = null;
        if (listOfMacros != null && listOfMacros.isDirectory()) {
            listOfFiles = listOfMacros.list();
        }
        ArrayList<String> fileList = new ArrayList<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].toLowerCase().endsWith(".txt")) {
                File tempFile = new File(dir + File.separator + listOfFiles[i]);
                if (!tempFile.isDirectory()) {
                    fileList.add(listOfFiles[i]);
                }
                tempFile = null;
            }
        }
        listOfFiles = new String[fileList.size()];
        for (int i = 0; i < fileList.size(); i++) {
            listOfFiles[i] = fileList.get(i);
        }
        fileList.clear();
        fileList = null;
        return listOfFiles;
    }

}
