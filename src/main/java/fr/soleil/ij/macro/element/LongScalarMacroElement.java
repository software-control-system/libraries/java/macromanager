package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.exception.MacroCompatibilityException;

/**
 * An element which value is of class Long
 * 
 * @author GIRARDOT
 */
public class LongScalarMacroElement extends DefaultMacroElement<Long> {

    public LongScalarMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.SCALAR_INT;
    }

    @Override
    protected Long cloneValue() {
        Long result;
        if (value == null) {
            result = null;
        } else {
            result = Long.valueOf(value.longValue());
        }
        return result;
    }

    @Override
    public boolean needAdapter() {
        return false;
    }

    @Override
    protected StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        return buffer.append(value);
    }

    @Override
    protected void writeAdapter() {
        // nothing to do
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            try {
                setValue(Long.valueOf(value));
            } catch (Exception e) {
                throw new MacroCompatibilityException("Bad formatted string value", e);
            }
        }
    }

    @Override
    public void unregisterContent() {
        // nothing to do: no adapter to unload
    }

    @Override
    public LongScalarMacroElement clone() {
        return (LongScalarMacroElement) super.clone();
    }
}
