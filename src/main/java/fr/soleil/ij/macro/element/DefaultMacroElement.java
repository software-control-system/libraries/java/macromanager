package fr.soleil.ij.macro.element;

import java.util.Random;

import fr.soleil.ij.macro.exception.MacroCompatibilityException;

/**
 * A class that gives some basic implementations of {@link IMacroElement}
 * 
 * @param <T> The class of this element's value
 * @author GIRARDOT
 */
public abstract class DefaultMacroElement<T> implements IMacroElement<T> {

    protected String name;

    protected T value;
    protected String key;

    public DefaultMacroElement() {
        name = null;
        key = null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return toStringBuilder(null).toString();
    }

    @Override
    public StringBuilder toStringBuilder(StringBuilder buffer) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        buffer.append(name);
        buffer.append(":");
        valueToStringBuilder(buffer);
        return buffer;
    }

    @Override
    public StringBuilder toWritableStringBuilder(StringBuilder buffer) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        buffer.append(name);
        buffer.append(":");
        if (needAdapter()) {
            if (getKey() == null) {
                setKey(generateKey());
            }
            buffer.append(key);
            writeAdapter();
        } else {
            valueToStringBuilder(buffer);
        }
        return buffer;
    }

    protected abstract T cloneValue();

    protected abstract void writeAdapter();

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String generateKey() {
        StringBuilder keyBuffer = new StringBuilder();
        keyBuffer.append(getName());
        keyBuffer.append("__");
        keyBuffer.append(Double.toString(new Random().nextDouble()));
        return keyBuffer.toString();
    }

    @Override
    public abstract boolean needAdapter();

    @Override
    public StringBuilder valueToStringBuilder(StringBuilder buffer) {
        if (buffer == null) {
            buffer = new StringBuilder();
        }
        return valueToStringBuilderNoCheck(buffer);
    }

    protected abstract StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer);

    public void updateFromStringRepresentation(String representation) throws MacroCompatibilityException {
        if ((representation == null) || representation.trim().isEmpty()) {
            throw new MacroCompatibilityException("Can not analyze an empty or null String");
        } else {
            int index = representation.indexOf(':');
            if (index < 0) {
                throw new MacroCompatibilityException("Bad formatted String representation: '" + representation + "'");
            } else {
                setName(representation.substring(0, index));
                updateValue(representation.substring(index + 1, representation.length()));
            }
        }
    }

    protected abstract void updateValue(String value) throws MacroCompatibilityException;

    /**
     * Method used to unregister the value associated with this element from the corresponding
     * adapter
     */
    public abstract void unregisterContent();

    @Override
    public DefaultMacroElement<T> clone() {
        DefaultMacroElement<T> clone;
        try {
            @SuppressWarnings("unchecked")
            DefaultMacroElement<T> element = (DefaultMacroElement<T>) super.clone();
            element.value = cloneValue();
            clone = element;
        } catch (CloneNotSupportedException e) {
            // Should not happen
            clone = this;
        }
        return clone;
    }

}
