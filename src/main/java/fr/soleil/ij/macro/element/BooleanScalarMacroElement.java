package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class Boolean
 * 
 * @author GIRARDOT
 */
public class BooleanScalarMacroElement extends DefaultMacroElement<Boolean> {

    public BooleanScalarMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.SCALAR_BOOLEAN;
    }

    @Override
    protected Boolean cloneValue() {
        Boolean result;
        if (value == null) {
            result = null;
        } else {
            result = Boolean.valueOf(value.booleanValue());
        }
        return result;
    }

    @Override
    public boolean needAdapter() {
        return false;
    }

    @Override
    protected StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        return buffer.append(value);
    }

    @Override
    protected void writeAdapter() {
        // nothing to do
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            try {
                setValue(Boolean.valueOf(MacroUtils.toBooleanFromString(value)));
            } catch (Exception e) {
                throw new MacroCompatibilityException("Bad formatted string value", e);
            }
        }
    }

    @Override
    public void unregisterContent() {
        // nothing to do: no adapter to unload
    }

    @Override
    public BooleanScalarMacroElement clone() {
        return (BooleanScalarMacroElement) super.clone();
    }
}
