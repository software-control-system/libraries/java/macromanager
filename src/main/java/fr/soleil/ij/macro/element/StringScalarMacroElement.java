package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.adapter.MacroStringScalarAdapter;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class String
 * 
 * @author GIRARDOT
 */
public class StringScalarMacroElement extends DefaultMacroElement<String> {

    public StringScalarMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.SCALAR_STRING;
    }

    @Override
    public void setValue(String value) {
        if (value != null) {
            value = value.replaceAll(MacroUtils.NEW_LINE_REPLACEMENT, MacroUtils.NEW_LINE).replaceAll(MacroUtils.SEMICOLON_REPLACEMENT, MacroUtils.SEMICOLON);
        }
        super.setValue(value);
    }

    @Override
    protected String cloneValue() {
        String result;
        if (value == null) {
            result = null;
        } else {
            result = new String(value);
        }
        return result;
    }

    @Override
    public StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        buffer.append(value == null ? value
                : value.replaceAll(MacroUtils.NEW_LINE, MacroUtils.NEW_LINE_REPLACEMENT).replaceAll(MacroUtils.SEMICOLON, MacroUtils.SEMICOLON_REPLACEMENT));
        return buffer;
    }

    @Override
    public boolean needAdapter() {
        return true;
    }

    @Override
    protected void writeAdapter() {
        MacroStringScalarAdapter.setString(getKey(), getValue());
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            setValue(MacroStringScalarAdapter.getString(value));
            setKey(value);
        }
    }

    @Override
    public void unregisterContent() {
        MacroStringScalarAdapter.deleteString(getKey());
    }

    @Override
    public StringScalarMacroElement clone() {
        return (StringScalarMacroElement) super.clone();
    }
}
