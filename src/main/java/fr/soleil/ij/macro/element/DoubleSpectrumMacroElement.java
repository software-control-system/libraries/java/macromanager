package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.adapter.MacroDoubleSpectrumAdapter;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class double[]
 * 
 * @author GIRARDOT
 */
public class DoubleSpectrumMacroElement extends DefaultMacroElement<double[]> {

    public DoubleSpectrumMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.SPECTRUM_FLOAT;
    }

    @Override
    protected double[] cloneValue() {
        return value == null ? null : value.clone();
    }

    @Override
    public boolean needAdapter() {
        return true;
    }

    @Override
    protected StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        if (value == null) {
            buffer.append(MacroUtils.NULL);
        } else {
            for (int i = 0; i < value.length; i++) {
                if (i > 0) {
                    buffer.append(';');
                }
                buffer.append(value[i]);
            }
        }
        return buffer;
    }

    @Override
    protected void writeAdapter() {
        MacroDoubleSpectrumAdapter.setSpectrum(getKey(), getValue());
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            setValue(MacroDoubleSpectrumAdapter.getSpectrum(value));
            setKey(value);
        }
    }

    @Override
    public void unregisterContent() {
        MacroDoubleSpectrumAdapter.deleteSpectrum(getKey());
    }

    @Override
    public DoubleSpectrumMacroElement clone() {
        return (DoubleSpectrumMacroElement) super.clone();
    }
}
