package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.adapter.MacroLongSpectrumAdapter;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class long[]
 * 
 * @author GIRARDOT
 */
public class LongSpectrumMacroElement extends DefaultMacroElement<long[]> {

    public LongSpectrumMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.SPECTRUM_INT;
    }

    @Override
    protected long[] cloneValue() {
        return value == null ? null : value.clone();
    }

    @Override
    public boolean needAdapter() {
        return true;
    }

    @Override
    protected StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        if (value == null) {
            buffer.append(MacroUtils.NULL);
        } else {
            for (int i = 0; i < value.length; i++) {
                if (i > 0) {
                    buffer.append(';');
                }
                buffer.append(value[i]);
            }
        }
        return buffer;
    }

    @Override
    protected void writeAdapter() {
        MacroLongSpectrumAdapter.setSpectrum(getKey(), getValue());
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            setValue(MacroLongSpectrumAdapter.getSpectrum(value));
            setKey(value);
        }
    }

    @Override
    public void unregisterContent() {
        MacroLongSpectrumAdapter.deleteSpectrum(getKey());
    }

    @Override
    public LongSpectrumMacroElement clone() {
        return (LongSpectrumMacroElement) super.clone();
    }
}
