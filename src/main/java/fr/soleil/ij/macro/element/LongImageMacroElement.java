package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.adapter.MacroLongImageAdapter;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class long[][]
 * 
 * @author GIRARDOT
 */
public class LongImageMacroElement extends DefaultMacroElement<long[][]> {

    public LongImageMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.IMAGE_INT;
    }

    @Override
    protected long[][] cloneValue() {
        long[][] result;
        if (value == null) {
            result = null;
        } else {
            if (value.length == 0) {
                result = new long[0][0];
            } else {
                result = new long[value.length][value[0].length];
                for (int i = 0; i < value.length; i++) {
                    for (int j = 0; j < value[i].length; j++) {
                        result[i][j] = value[i][j];
                    }
                }
            }
        }
        return result;
    }

    @Override
    public boolean needAdapter() {
        return true;
    }

    @Override
    public StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        if (value == null) {
            buffer.append(MacroUtils.NULL);
        } else {
            for (int i = 0; i < value.length; i++) {
                for (int j = 0; j < value[i].length; j++) {
                    buffer.append(value[i][j]);
                    if (j < value[i].length - 1) {
                        buffer.append(MacroUtils.SEMICOLON);
                    }
                }
                if (i < value.length - 1) {
                    buffer.append(MacroUtils.SLASH);
                }
            }
        }
        return buffer;
    }

    @Override
    protected void writeAdapter() {
        MacroLongImageAdapter.setImage(getKey(), getValue());
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            setValue(MacroLongImageAdapter.getImage(value));
            setKey(value);
        }
    }

    @Override
    public void unregisterContent() {
        MacroLongImageAdapter.deleteImage(getKey());
    }

    @Override
    public LongImageMacroElement clone() {
        return (LongImageMacroElement) super.clone();
    }
}
