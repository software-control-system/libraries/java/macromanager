package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.adapter.MacroStringSpectrumAdapter;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class String
 * 
 * @author GIRARDOT
 */
public class StringSpectrumMacroElement extends DefaultMacroElement<String[]> {

    public StringSpectrumMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.SPECTRUM_STRING;
    }

    @Override
    public void setValue(String[] value) {
        if (value != null) {
            for (int i = 0; i < value.length; i++) {
                if (value[i] != null) {
                    value[i] = value[i].replaceAll(MacroUtils.NEW_LINE_REPLACEMENT, MacroUtils.NEW_LINE).replaceAll(MacroUtils.SEMICOLON_REPLACEMENT, MacroUtils.SEMICOLON);
                }
            }
        }
        super.setValue(value);
    }

    @Override
    protected String[] cloneValue() {
        return value == null ? null : value.clone();
    }

    @Override
    public StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        if (value == null) {
            buffer.append(MacroUtils.NULL);
        } else {
            for (int i = 0; i < value.length; i++) {
                if (i > 0) {
                    buffer.append(MacroUtils.SEMICOLON);
                }
                buffer.append(value[i] == null ? MacroUtils.NULL
                        : value[i].replaceAll(MacroUtils.NEW_LINE, MacroUtils.NEW_LINE_REPLACEMENT).replaceAll(MacroUtils.SEMICOLON, MacroUtils.SEMICOLON_REPLACEMENT));
            }
        }
        return buffer;
    }

    @Override
    public boolean needAdapter() {
        return true;
    }

    @Override
    protected void writeAdapter() {
        MacroStringSpectrumAdapter.setSpectrum(getKey(), getValue());
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            setValue(MacroStringSpectrumAdapter.getSpectrum(value));
            setKey(value);
        }
    }

    @Override
    public void unregisterContent() {
        MacroStringSpectrumAdapter.deleteSpectrum(getKey());
    }

    @Override
    public StringSpectrumMacroElement clone() {
        return (StringSpectrumMacroElement) super.clone();
    }
}
