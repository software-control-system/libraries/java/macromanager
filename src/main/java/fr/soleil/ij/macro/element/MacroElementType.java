package fr.soleil.ij.macro.element;

/**
 * An {@link Enum} that represents the kind of types available for macro
 * elements
 * 
 * @author GIRARDOT
 */
public enum MacroElementType {
    SCALAR_INT("int scalar", "a scalar with a long value", "intscalar"),
    SCALAR_FLOAT("float scalar", "a scalar with a double value", "floatscalar"),
    SCALAR_BOOLEAN("boolean scalar", "a scalar with a boolean value", "booleanscalar"),
    SCALAR_STRING("String scalar", "a String", "stringscalar"),
    SPECTRUM_INT("int spectrum", "a spectrum with long values", "intspectrum"),
    SPECTRUM_FLOAT("float spectrum", "a spectrum with double values", "floatspectrum"),
    SPECTRUM_BOOLEAN("boolean spectrum", "a spectrum with boolean values", "booleanspectrum"),
    SPECTRUM_STRING("String spectrum", "a spectrum with String values", "stringspectrum"),
    IMAGE_INT("int image", "a matrix with long values", "intimage"),
    IMAGE_FLOAT("float image", "a matrix with double values", "floatimage"),
    IMAGE_BOOLEAN("boolean image", "a matrix with boolean values", "booleanimage"),
    IMAGE_STRING("String image", "a matrix with String values", "stringimage");

    private String name;
    private String description;
    private String macroKey;

    private MacroElementType(String name, String description, String key) {
        this.name = name;
        this.description = description;
        this.macroKey = key;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getMacroKey() {
        return macroKey;
    }

}
