package fr.soleil.ij.macro.element;

/**
 * An interface that represents an element a macro can interact with.
 * 
 * @param <T>
 *            The class of this element's value
 * @author GIRARDOT
 */
public interface IMacroElement<T> extends Cloneable {

    public String getName();

    public void setName(String name);

    public T getValue();

    public void setValue(T value);

    /**
     * Appends this {@link IMacroElement} {@link String} representation in a {@link StringBuilder}
     * 
     * @param buffer a {@link StringBuilder} to which to append the {@link String} representation. Can be
     *            <code>null</code>.
     * @return the {@link StringBuilder} containing the {@link String} representation. If <code>buffer</code> is
     *         <code>null</code>, a new {@link StringBuilder} is returned. Otherwise, <code>buffer</code> is returned,
     *         and the {@link String} representation is appended to it.
     */
    public StringBuilder toStringBuilder(StringBuilder buffer);

    /**
     * Appends this {@link IMacroElement} writable {@link String} representation in a {@link StringBuilder} (the
     * {@link String} representation that can be used as a macro parameter)
     * 
     * @param buffer a {@link StringBuilder} to which to append the {@link String}
     *            representation. Can be <code>null</code>.
     * @return the {@link StringBuilder} containing the {@link String} representation. If <code>buffer</code> is
     *         <code>null</code>, a new {@link StringBuilder} is returned. Otherwise, <code>buffer</code> is returned,
     *         and the {@link String} representation is appended to it.
     */
    public StringBuilder toWritableStringBuilder(StringBuilder buffer);

    /**
     * Appends this {@link IMacroElement}'s value {@link String} representation
     * in a {@link StringBuilder}
     * 
     * @param buffer a {@link StringBuilder} to which to append the {@link String} representation. Can be
     *            <code>null</code>.
     * @return the {@link StringBuilder} containing the {@link String} representation. If <code>buffer</code> is
     *         <code>null</code>, a new {@link StringBuilder} is returned. Otherwise, <code>buffer</code> is returned,
     *         and the {@link String} representation is appended to it.
     */
    public StringBuilder valueToStringBuilder(StringBuilder buffer);

    /**
     * Returns the type of this {@link IMacroElement}
     * 
     * @return an {@link MacroElementType}
     */
    public MacroElementType getElementType();

    /**
     * Returns the key that can be used with adapters
     * 
     * @return a {@link String}
     */
    public String getKey();

    /**
     * Forces this {@link IMacroElement} to set its key with a particular value
     * 
     * @param key the value to set to the key
     */
    public void setKey(String key);

    /**
     * Generates a key that can be used with adapters
     * 
     * @return a {@link String}
     */
    public String generateKey();

    /**
     * A boolean value to know whether this {@link IMacroElement} needs an adapter to manage its value.
     * 
     * @return <code>true</code> if an adapter is needed, <code>false</code> otherwise
     */
    public boolean needAdapter();

}
