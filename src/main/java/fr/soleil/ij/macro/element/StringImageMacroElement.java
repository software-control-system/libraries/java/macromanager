package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.adapter.MacroStringImageAdapter;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class String[][]
 * 
 * @author GIRARDOT
 */
public class StringImageMacroElement extends DefaultMacroElement<String[][]> {

    public StringImageMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.IMAGE_STRING;
    }

    @Override
    protected String[][] cloneValue() {
        String[][] result;
        if (value == null) {
            result = null;
        } else {
            if (value.length == 0) {
                result = new String[0][0];
            } else {
                result = new String[value.length][value[0].length];
                for (int i = 0; i < value.length; i++) {
                    for (int j = 0; j < value[i].length; j++) {
                        result[i][j] = value[i][j];
                    }
                }
            }
        }
        return result;
    }

    @Override
    public boolean needAdapter() {
        return true;
    }

    @Override
    public StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        if (value == null) {
            buffer.append(MacroUtils.NULL);
        } else {
            for (int i = 0; i < value.length; i++) {
                for (int j = 0; j < value[i].length; j++) {
                    if (value[i][j] == null) {
                        buffer.append(value[i][j]);
                    } else {
                        buffer.append(value[i][j].replaceAll(MacroUtils.SEMICOLON, MacroUtils.SEMICOLON_REPLACEMENT)
                                .replaceAll(MacroUtils.NEW_LINE, MacroUtils.NEW_LINE_REPLACEMENT).replaceAll(MacroUtils.SLASH, MacroUtils.SLASH_REPLACEMENT));
                    }
                    if (j < value[i].length - 1) {
                        buffer.append(MacroUtils.SEMICOLON);
                    }
                }
                if (i < value.length - 1) {
                    buffer.append(MacroUtils.SLASH);
                }
            }
        }
        return buffer;
    }

    @Override
    protected void writeAdapter() {
        MacroStringImageAdapter.setImage(getKey(), getValue());
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            setValue(MacroStringImageAdapter.getImage(value));
            setKey(value);
        }
    }

    @Override
    public void unregisterContent() {
        MacroStringImageAdapter.deleteImage(getKey());
    }

    @Override
    public StringImageMacroElement clone() {
        return (StringImageMacroElement) super.clone();
    }
}
