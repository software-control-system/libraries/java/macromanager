package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.exception.MacroCompatibilityException;

/**
 * An element which value is of class Double
 * 
 * @author GIRARDOT
 */
public class DoubleScalarMacroElement extends DefaultMacroElement<Double> {

    public DoubleScalarMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.SCALAR_FLOAT;
    }

    @Override
    protected Double cloneValue() {
        Double result;
        if (value == null) {
            result = null;
        } else {
            result = Double.valueOf(value.doubleValue());
        }
        return result;
    }

    @Override
    public boolean needAdapter() {
        return false;
    }

    @Override
    protected StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        return buffer.append(value);
    }

    @Override
    protected void writeAdapter() {
        // nothing to do
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            try {
                setValue(Double.valueOf(value));
            } catch (Exception e) {
                throw new MacroCompatibilityException("Bad formatted string value", e);
            }
        }
    }

    @Override
    public void unregisterContent() {
        // nothing to do: no adapter to unload
    }

    @Override
    public DoubleScalarMacroElement clone() {
        return (DoubleScalarMacroElement) super.clone();
    }
}
