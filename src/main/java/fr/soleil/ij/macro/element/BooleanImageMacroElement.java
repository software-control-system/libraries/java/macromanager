package fr.soleil.ij.macro.element;

import fr.soleil.ij.macro.adapter.MacroBooleanImageAdapter;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * An element which value is of class boolean[][]
 * 
 * @author GIRARDOT
 */
public class BooleanImageMacroElement extends DefaultMacroElement<boolean[][]> {

    public BooleanImageMacroElement() {
        super();
    }

    @Override
    public MacroElementType getElementType() {
        return MacroElementType.IMAGE_BOOLEAN;
    }

    @Override
    protected boolean[][] cloneValue() {
        boolean[][] result;
        if (value == null) {
            result = null;
        } else {
            if (value.length == 0) {
                result = new boolean[0][0];
            } else {
                result = new boolean[value.length][];
                for (int i = 0; i < value.length; i++) {
                    boolean[] val = value[i];
                    if (val != null) {
                        result[i] = val.clone();
                    }
                }
            }
        }
        return result;
    }

    @Override
    public boolean needAdapter() {
        return true;
    }

    @Override
    public StringBuilder valueToStringBuilderNoCheck(StringBuilder buffer) {
        if (value == null) {
            buffer.append(MacroUtils.NULL);
        } else {
            for (int i = 0; i < value.length; i++) {
                for (int j = 0; j < value[i].length; j++) {
                    buffer.append(value[i][j]);
                    if (j < value[i].length - 1) {
                        buffer.append(MacroUtils.SEMICOLON);
                    }
                }
                if (i < value.length - 1) {
                    buffer.append(MacroUtils.SLASH);
                }
            }
        }
        return buffer;
    }

    @Override
    protected void writeAdapter() {
        MacroBooleanImageAdapter.setImage(getKey(), getValue());
    }

    @Override
    protected void updateValue(String value) throws MacroCompatibilityException {
        if ((value == null) || value.trim().isEmpty()) {
            throw new MacroCompatibilityException("null or empty string value");
        } else {
            setValue(MacroBooleanImageAdapter.getImage(value));
            setKey(value);
        }
    }

    @Override
    public void unregisterContent() {
        MacroBooleanImageAdapter.deleteImage(getKey());
    }

    @Override
    public BooleanImageMacroElement clone() {
        return (BooleanImageMacroElement) super.clone();
    }
}
