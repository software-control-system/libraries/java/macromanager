package fr.soleil.ij.macro;

import java.io.File;

import fr.soleil.ij.macro.adapter.MacroBooleanImageAdapter;
import fr.soleil.ij.macro.adapter.MacroBooleanSpectrumAdapter;
import fr.soleil.ij.macro.adapter.MacroDoubleImageAdapter;
import fr.soleil.ij.macro.adapter.MacroDoubleSpectrumAdapter;
import fr.soleil.ij.macro.adapter.MacroLongImageAdapter;
import fr.soleil.ij.macro.adapter.MacroLongSpectrumAdapter;
import fr.soleil.ij.macro.adapter.MacroStringImageAdapter;
import fr.soleil.ij.macro.adapter.MacroStringSpectrumAdapter;
import fr.soleil.ij.macro.element.BooleanImageMacroElement;
import fr.soleil.ij.macro.element.BooleanScalarMacroElement;
import fr.soleil.ij.macro.element.BooleanSpectrumMacroElement;
import fr.soleil.ij.macro.element.DoubleImageMacroElement;
import fr.soleil.ij.macro.element.DoubleScalarMacroElement;
import fr.soleil.ij.macro.element.DoubleSpectrumMacroElement;
import fr.soleil.ij.macro.element.LongImageMacroElement;
import fr.soleil.ij.macro.element.LongScalarMacroElement;
import fr.soleil.ij.macro.element.LongSpectrumMacroElement;
import fr.soleil.ij.macro.element.MacroElementType;
import fr.soleil.ij.macro.element.StringImageMacroElement;
import fr.soleil.ij.macro.element.StringScalarMacroElement;
import fr.soleil.ij.macro.element.StringSpectrumMacroElement;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.exception.MacroHeaderException;
import fr.soleil.ij.macro.interactable.MacroInteractable;
import fr.soleil.ij.macro.interactable.MacroInteractableList;
import fr.soleil.ij.macro.interactable.MacroInteractableSingle;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * Class used to hold the structure to interact with an ImageJ macro. This is the entry point for
 * any user of this library.
 * 
 * @author GIRARDOT
 */
public class Macro extends AbstractRunnableMacro {

    private final boolean file;

    /**
     * Analyzes a macro and creates its corresponding structure. Please prefer using this constructor than the other
     * one.
     * 
     * @param macro The macro to analyze
     * @param file a boolean to tell whether <code>macro</code> represents a {@link File} path or directly the macro
     *            code itself. <code>true</code> to make the constructor consider <code>macro</code> as a {@link File}
     *            path.
     * @throws MacroHeaderException If a problem happened while analyzing the macro header, which means that the header
     *             was not well formatted.
     */
    public Macro(String macro, boolean file) throws MacroHeaderException {
        super(macro);
        this.file = file;
        try {
            analyzeMacro();
        } finally {
            unregisterInteractables();
        }
    }

    /**
     * Analyzes a macro and creates its corresponding structure. Please prefer using the other constructor.
     * 
     * @param macro The macro to analyze. This constructor will first try to consider it as a {@link File} path. If an
     *            <u>existing</u> {@link File}, which path is this argument, is found, <code>macro</code> is considered
     *            as a {@link File} path. Otherwise, it is considered as a macro code.
     * @throws MacroHeaderException If a problem happened while analyzing the macro header, which means that the header
     *             was not well formatted.
     */
    public Macro(String macro) throws MacroHeaderException {
        this(macro, checkIsFile(macro));
    }

    /**
     * Tells whether the associated macro is a {@link File} or some ImageJ macro code
     * 
     * @return A boolean value. <code>true</code> if the macro is a {@link File}
     */
    public boolean isFile() {
        return file;
    }

    /**
     * Runs the macro, and finishes building the results (sets the missing values)
     * 
     * @throws MacroCompatibilityException If the result(s) of the macro is/are not compatible with what the macro
     *             declared in header.
     */
    @Override
    public void run() throws MacroCompatibilityException {
        try {
            MacroDoubleSpectrumAdapter.initPluginConnexion();
            MacroLongSpectrumAdapter.initPluginConnexion();
            MacroBooleanSpectrumAdapter.initPluginConnexion();
            MacroStringSpectrumAdapter.initPluginConnexion();
            MacroDoubleImageAdapter.initPluginConnexion();
            MacroLongImageAdapter.initPluginConnexion();
            MacroBooleanImageAdapter.initPluginConnexion();
            MacroStringImageAdapter.initPluginConnexion();
            StringBuilder argBuffer = new StringBuilder();
            if (parameters != null) {
                for (int i = 0; i < parameters.length; i++) {
                    parameters[i].toWritableStringBuilder(argBuffer);
                    if (i < parameters.length - 1) {
                        argBuffer.append(MacroController.MACRO_INTERACTABLE_SEPARATOR);
                    }
                }
            }
            String macroResult;
            if (isFile()) {
                macroResult = MacroController.runMacroFile(macro, argBuffer.toString());
            } else {
                macroResult = MacroController.runMacro(macro, argBuffer.toString());
            }
            if ((results != null) && (results.length > 0)) {
                // Some results are expected
                if ((macroResult == null) || macroResult.trim().isEmpty()) {
                    // The macro returned no result
                    throw new MacroCompatibilityException("No valid result returned by macro");
                } else {
                    // The macro did return a result --> check compatibility
                    if (results.length == 1) {
                        // Only 1 result is expected
                        updateInteractable(results[0], macroResult, "result");
                    } else {
                        // More than 1 results are expected
                        String[] separatedMacroResults = macroResult
                                .split(MacroController.MACRO_INTERACTABLE_SEPARATOR);
                        if (separatedMacroResults.length == results.length) {
                            for (int i = 0; i < results.length; i++) {
                                updateInteractable(results[i], separatedMacroResults[i], "result");
                            }
                        } else {
                            // invalid result count
                            StringBuilder errorBuffer = new StringBuilder();
                            errorBuffer.append("The macro did not return ");
                            errorBuffer.append("the expected count of results:\n");
                            errorBuffer.append("It returned ");
                            errorBuffer.append(separatedMacroResults.length);
                            errorBuffer.append(" results instead of ");
                            errorBuffer.append(results.length);
                            throw new MacroCompatibilityException(errorBuffer.toString());
                        }
                    }
                }
            }
        } finally {
            unregisterInteractables();
        }
    }

    /**
     * Calls the MacroHeaderExtractor to extract keys and values from the macro, and then analyzes them.
     */
    private void analyzeMacro() throws MacroHeaderException {
        if (isFile()) {
            extractor.extractMacroFileHead(macro);
        } else {
            extractor.extractMacroHead(macro);
        }
        header.clear();
        extractor.fillHeaderMap(header);
        description = extractor.getValueFor("description");
        parameters = analyzeInteractable("parameter");
        results = analyzeInteractable("result");
    }

    /**
     * Analyzes the keys and values to create the corresponding parameters/results structure. As results and parameters
     * are organized quite the same way, an argument is needed to know whether parameters or results should be analyzed.
     * 
     * @param kindOfInteractable The kind of interactables to analyze. "parameter" for parameters, "result" for results
     * @return The corresponding structure, represented as a {@link MacroInteractable}[]
     * @throws MacroHeaderException if a problem occurred during analysis
     */
    private MacroInteractable<?>[] analyzeInteractable(String kindOfInteractable) throws MacroHeaderException {
        int count = 0;
        try {
            count = Integer.parseInt(extractor.getValueFor(kindOfInteractable + "_count"));
        } catch (Exception e) {
            count = 0;
        }
        if (count < 0) {
            count = 0;
        }
        MacroInteractable<?>[] result = new MacroInteractable<?>[count];
        String key = null;
        String type = null;
        String cardinality = null;
        String description = null;
        String displayShortName = null;
        String displayName = null;
        for (int i = 0; i < count; i++) {
            key = kindOfInteractable + MacroUtils.KEY_SEPARATOR + i;
            type = extractor.getValueFor(key + "_type");
            cardinality = extractor.getValueFor(key + "_cardinality");
            description = extractor.getValueFor(key + "_description");
            displayShortName = extractor.getValueFor(key + "_displayShortName");
            displayName = extractor.getValueFor(key + "_displayName");
            if ((type == null) || (cardinality == null)) {
                StringBuilder errorBuffer = new StringBuilder();
                errorBuffer.append("Header badly formatted : ");
                errorBuffer.append("not enough information to build ");
                errorBuffer.append(kindOfInteractable).append(MacroUtils.SPACE);
                errorBuffer.append(i);
                throw new MacroHeaderException(errorBuffer.toString());
            } else {
                type = type.trim();
                cardinality = cardinality.trim();
                try {
                    if (type.equalsIgnoreCase(MacroElementType.SCALAR_INT.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<LongScalarMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SCALAR_INT);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<LongScalarMacroElement>(description, displayShortName,
                                    displayName, MacroElementType.SCALAR_INT);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.SCALAR_FLOAT.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<DoubleScalarMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SCALAR_FLOAT);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<DoubleScalarMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SCALAR_FLOAT);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.SCALAR_BOOLEAN.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<BooleanScalarMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SCALAR_BOOLEAN);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<BooleanScalarMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SCALAR_BOOLEAN);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.SCALAR_STRING.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<StringScalarMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SCALAR_STRING);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<StringScalarMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SCALAR_STRING);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.SPECTRUM_INT.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<LongSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_INT);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<LongSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_INT);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.SPECTRUM_FLOAT.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<DoubleSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_FLOAT);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<DoubleSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_FLOAT);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.SPECTRUM_BOOLEAN.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<BooleanSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_BOOLEAN);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<BooleanSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_BOOLEAN);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.SPECTRUM_STRING.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<StringSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_STRING);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<StringSpectrumMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.SPECTRUM_STRING);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.IMAGE_INT.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<LongImageMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.IMAGE_INT);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<LongImageMacroElement>(description, displayShortName,
                                    displayName, MacroElementType.IMAGE_INT);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.IMAGE_FLOAT.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<DoubleImageMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.IMAGE_FLOAT);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<DoubleImageMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.IMAGE_FLOAT);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.IMAGE_BOOLEAN.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<BooleanImageMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.IMAGE_BOOLEAN);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<BooleanImageMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.IMAGE_BOOLEAN);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else if (type.equalsIgnoreCase(MacroElementType.IMAGE_STRING.getMacroKey())) {
                        if (cardinality.equalsIgnoreCase("1")) {
                            result[i] = new MacroInteractableSingle<StringImageMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.IMAGE_STRING);
                        } else if (cardinality.equalsIgnoreCase("n")) {
                            result[i] = new MacroInteractableList<StringImageMacroElement>(description,
                                    displayShortName, displayName, MacroElementType.IMAGE_STRING);
                        } else {
                            throw generateCardinalityException(i, cardinality, kindOfInteractable);
                        }
                    } else {
                        throw generateTypeException(i, type, kindOfInteractable);
                    }
                } catch (MacroCompatibilityException mce) {
                    // Should never happen, because this Exception only occurs when the
                    // MacroElementType is null in MacroInteractable constructor, which is never the
                    // case.
                    throw new MacroHeaderException(mce.getMessage(), mce);
                }
            }
        }
        return result;
    }

    @Override
    public Macro clone() {
        return (Macro) super.clone();
    }

}
