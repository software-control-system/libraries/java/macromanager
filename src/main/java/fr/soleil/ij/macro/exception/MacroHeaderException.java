package fr.soleil.ij.macro.exception;

/**
 * An exception that can occur on analyzing a Macro Header
 * 
 * @author GIRARDOT
 */
public class MacroHeaderException extends MacroException {

    private static final long serialVersionUID = 3473266539899758103L;

    public MacroHeaderException() {
        super();
    }

    public MacroHeaderException(String message) {
        super(message);
    }

    public MacroHeaderException(Throwable cause) {
        super(cause);
    }

    public MacroHeaderException(String message, Throwable cause) {
        super(message, cause);
    }

}
