package fr.soleil.ij.macro.exception;

public abstract class MacroException extends Exception {

    private static final long serialVersionUID = -6375183740469984822L;

    public MacroException() {
        super();
    }

    public MacroException(String message) {
        super(message);
    }

    public MacroException(Throwable cause) {
        super(cause);
    }

    public MacroException(String message, Throwable cause) {
        super(message, cause);
    }

    public MacroException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
