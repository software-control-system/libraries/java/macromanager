package fr.soleil.ij.macro.exception;

/**
 * An exception that can occur on interacting with
 * {@link fr.soleil.ij.macro.element.IMacroElement} or with
 * {@link fr.soleil.ij.macro.interactable.MacroInteractable}
 * 
 * @author GIRARDOT
 */
public class MacroCompatibilityException extends MacroException {

    private static final long serialVersionUID = -8734394988605723116L;

    public MacroCompatibilityException() {
        super();
    }

    public MacroCompatibilityException(String message) {
        super(message);
    }

    public MacroCompatibilityException(Throwable cause) {
        super(cause);
    }

    public MacroCompatibilityException(String message, Throwable cause) {
        super(message, cause);
    }

}
