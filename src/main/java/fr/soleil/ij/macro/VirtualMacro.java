package fr.soleil.ij.macro;

import fr.soleil.ij.macro.interactable.MacroInteractable;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A virtual representation of a macro. Allows users to generate a macro representation, without
 * needing macro existence
 *
 * @author girardot
 */
public class VirtualMacro extends AbstractMacro {

    /**
     * Creates a new {@link VirtualMacro} with all necessary information
     *
     * @param macro The theoric macro
     * @param description The desired macro description
     * @param header The desired macro header
     * @param parameters The desired macro parameters
     * @param results The desired macro results
     */
    public VirtualMacro(String macro, String description, Map<String, String> header,
            MacroInteractable<?>[] parameters, MacroInteractable<?>[] results) {
        super(macro);
        this.description = description;
        this.parameters = parameters;
        this.results = results;
        if (header != null) {
            for (Entry<String, String> entry : header.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if ((key != null) && (value != null)) {
                    this.header.put(key, value);
                }
            }
        }
    }

    public VirtualMacro clone() {
        return (VirtualMacro) super.clone();
    }

}
