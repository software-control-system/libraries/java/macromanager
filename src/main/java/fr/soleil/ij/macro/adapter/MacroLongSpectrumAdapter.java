package fr.soleil.ij.macro.adapter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.ij.macro.util.MacroUtils;
import ij.IJ;
import ij.macro.ExtensionDescriptor;
import ij.macro.Functions;
import ij.macro.MacroExtension;
import ij.plugin.PlugIn;

public class MacroLongSpectrumAdapter implements IMacroAdapter, PlugIn, MacroExtension {

    protected static final String GET_LONG_VALUE_AT_SELECTED_SPECTRUM_INDEX = "getLongValueAtSelectedSpectrumIndex";
    protected static final String GET_LONG_SPECTRUM_INDEX = "getLongSpectrumIndex";
    protected static final String SET_LONG_SPECTRUM_INDEX = "setLongSpectrumIndex";
    protected static final String GET_LONG_SPECTRUM_KEY = "getLongSpectrumKey";
    protected static final String SET_LONG_SPECTRUM_KEY = "setLongSpectrumKey";
    protected static final String SET_LONG_SPECTRUM = "setLongSpectrum";
    protected static final String GET_LONG_SPECTRUM_LENGTH = "getLongSpectrumLength";
    protected static final String DELETE_LONG_SPECTRUM = "deleteLongSpectrum";

    protected final static Map<String, long[]> SPECTRUM_RW_TABLE = new ConcurrentHashMap<>();
    protected static long[] currentSpectrum = null;
    protected static String currentKey = null;
    protected static int currentIndex = 0;

    private static MacroLongSpectrumAdapter adapter = null;

    private ExtensionDescriptor[] extensions = {
            ExtensionDescriptor.newDescriptor(GET_LONG_VALUE_AT_SELECTED_SPECTRUM_INDEX, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_LONG_SPECTRUM_INDEX, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(SET_LONG_SPECTRUM_INDEX, this, ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_LONG_SPECTRUM_KEY, this, ARG_OUTPUT + ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_LONG_SPECTRUM_KEY, this, ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_LONG_SPECTRUM, this, ARG_ARRAY),
            ExtensionDescriptor.newDescriptor(GET_LONG_SPECTRUM_LENGTH, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(DELETE_LONG_SPECTRUM, this) };

    public static void initPluginConnexion() {
        if (adapter == null) {
            adapter = new MacroLongSpectrumAdapter();
        }
    }

    public MacroLongSpectrumAdapter() {
        super();
        try {
            IJ.getClassLoader().loadClass(MacroLongSpectrumAdapter.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String writeSpectrum(String key, String value) {
        String result;
        if (key == null || value == null) {
            result = KO_RESULT;
        } else {
            key = key.trim();
            String[] stringArray = value.split(MacroUtils.SEMICOLON);
            long[] spectrum = new long[stringArray.length];
            for (int i = 0; i < stringArray.length; i++) {
                try {
                    spectrum[i] = Long.parseLong(stringArray[i]);
                } catch (Exception e) {
                    spectrum[i] = 0;
                }
            }
            result = OK_RESULT;
        }
        return result;
    }

    public static void setSpectrum(String key, long[] value) {
        if (key != null) {
            key = key.trim();
            if (value == null) {
                SPECTRUM_RW_TABLE.remove(key);
            } else {
                SPECTRUM_RW_TABLE.put(key, value);
            }
        }
    }

    public static String deleteSpectrum(String key) {
        String result;
        if (key != null) {
            SPECTRUM_RW_TABLE.remove(key.trim());
            result = OK_RESULT;
        } else {
            result = KO_RESULT;
        }
        return result;
    }

    public static long[] getSpectrum(String key) {
        long[] result;
        if (key != null) {
            key = key.trim();
            result = SPECTRUM_RW_TABLE.get(key);
        } else {
            result = null;
        }
        return result;
    }

    public static String getSpectrumAsString(String key) {
        StringBuilder resultBuffer = new StringBuilder();
        if (key != null) {
            key = key.trim();
            long[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                for (int i = 0; i < spectrum.length; i++) {
                    if (i > 0) {
                        resultBuffer.append(MacroUtils.SEMICOLON);
                    }
                    resultBuffer.append(Long.toString(spectrum[i]));
                }
            }
            spectrum = null;
        }
        return resultBuffer.toString();
    }

    public static int getLength(String key) {
        int result = 0;
        if (key != null) {
            key = key.trim();
            long[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                result = spectrum.length;
                spectrum = null;
            }
        }
        return result;
    }

    public static String getMaximum(String key) {
        String result = MacroUtils.EMPTY_STRING;
        if (key != null) {
            key = key.trim();
            long[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                if (spectrum.length > 0) {
                    long max = Long.MIN_VALUE;
                    for (int i = 0; i < spectrum.length; i++) {
                        if (spectrum[i] > max) {
                            max = spectrum[i];
                        }
                    }
                }
                spectrum = null;
            }
        }
        return result;
    }

    public static String getMinimum(String key) {
        String result = MacroUtils.EMPTY_STRING;
        if (key != null) {
            key = key.trim();
            long[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                if (spectrum.length > 0) {
                    long min = Long.MAX_VALUE;
                    for (int i = 0; i < spectrum.length; i++) {
                        if (spectrum[i] < min) {
                            min = spectrum[i];
                        }
                    }
                }
                spectrum = null;
            }
        }
        return result;
    }

    public static double getMean(String key) {
        double result = Double.NaN;
        if (key != null) {
            key = key.trim();
            long[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                if (spectrum.length > 0) {
                    long cumul = 0;
                    for (int i = 0; i < spectrum.length; i++) {
                        cumul += spectrum[i];
                    }
                    result = cumul / (double) spectrum.length;
                }
                spectrum = null;
            }
        }
        return result;
    }

    public static String getValueAt(String key, String index) {
        String result = MacroUtils.EMPTY_STRING;
        if (key != null) {
            int i = -1;
            long[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                try {
                    i = Integer.parseInt(index);
                } catch (Exception e) {
                    i = -1;
                }
                if (i > -1 && i < spectrum.length) {
                    result = result + spectrum[i];
                }
            }
        }
        return result;
    }

    @Override
    public void run(String arg0) {
        Functions.registerExtensions(this);
    }

    @Override
    public ExtensionDescriptor[] getExtensionFunctions() {
        return extensions;
    }

    @Override
    public String handleExtension(String name, Object[] args) {
        switch (name) {
            case GET_LONG_VALUE_AT_SELECTED_SPECTRUM_INDEX:
                Double value;
                if ((currentSpectrum == null) || (currentIndex < 0) || (currentIndex >= currentSpectrum.length)) {
                    value = Double.valueOf(Double.NaN);
                } else {
                    value = Double.valueOf(currentSpectrum[currentIndex]);
                }
                ((Double[]) args[0])[0] = value;
                value = null;
                break;
            case GET_LONG_SPECTRUM_INDEX:
                ((Double[]) args[0])[0] = Double.valueOf(currentIndex);
                break;
            case SET_LONG_SPECTRUM_INDEX:
                currentIndex = ((Double) args[0]).intValue();
                break;
            case GET_LONG_SPECTRUM_KEY:
                ((String[]) args[0])[0] = (currentKey == null ? MacroUtils.EMPTY_STRING : currentKey);
                break;
            case SET_LONG_SPECTRUM_KEY:
                currentKey = (String) args[0];
                if (currentKey != null) {
                    currentKey = currentKey.trim();
                }
                currentSpectrum = getSpectrum(currentKey);
                currentIndex = 0;
                break;
            case SET_LONG_SPECTRUM:
                if (currentKey != null) {
                    Object[] temp = (Object[]) args[0];
                    if (temp != null) {
                        long[] spectrum = new long[temp.length];
                        for (int i = 0; i < temp.length; i++) {
                            if (temp[i] instanceof Double) {
                                spectrum[i] = ((Double) temp[i]).longValue();
                            } else {
                                spectrum[i] = 0;
                            }
                        }
                        setSpectrum(currentKey, spectrum);
                        currentSpectrum = spectrum;
                        spectrum = null;
                    }
                    temp = null;
                }
                break;
            case GET_LONG_SPECTRUM_LENGTH:
                int length = 0;
                if (currentSpectrum != null) {
                    length = currentSpectrum.length;
                }
                ((Double[]) args[0])[0] = Double.valueOf(length);
                break;
            case DELETE_LONG_SPECTRUM:
                currentSpectrum = null;
                currentIndex = 0;
                deleteSpectrum(currentKey);
                break;
        }
        return null;
    }

    public static void clean() {
        SPECTRUM_RW_TABLE.clear();
    }

}
