package fr.soleil.ij.macro.adapter;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;

import fr.soleil.ij.macro.util.MacroUtils;

public class MacroFileAdapter implements IMacroAdapter {

    public static int getLineCount(String filePath) {
        int count = 0;
        if (filePath != null && !filePath.trim().isEmpty()) {
            File file = new File(filePath);
            LineNumberReader reader = null;
            if (file.exists() && !file.isDirectory()) {
                try {
                    FileReader fr = new FileReader(file);
                    reader = new LineNumberReader(fr);
                    while (reader.readLine() != null) {
                        count++;
                    }
                } catch (Exception e) {
                    // NOTHING TO DO;
                    count = 0;
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e) {
                            // Nothing particular to do
                        }
                    }
                }
            }
        }
        return count;
    }

    public static String getLine(String filePath, String lineIndex) {
        String line;
        int index = -1;
        try {
            index = Integer.parseInt(lineIndex);
        } catch (Exception e) {
            index = -1;
            line = MacroUtils.EMPTY_STRING;
        }
        if (index < 0) {
            line = MacroUtils.EMPTY_STRING;
        } else if ((filePath == null) || filePath.trim().isEmpty()) {
            line = MacroUtils.EMPTY_STRING;
        } else {
            File file = new File(filePath);
            if (file.exists() && !file.isDirectory()) {
                LineNumberReader reader = null;
                try {
                    FileReader fr = new FileReader(file);
                    reader = new LineNumberReader(fr);
                    int count = -1;
                    line = MacroUtils.EMPTY_STRING;
                    while ((count < index) && (line != null)) {
                        line = reader.readLine();
                        count++;
                    }
                    if (line == null) {
                        line = MacroUtils.EMPTY_STRING;
                    }
                } catch (Exception e) {
                    // NOTHING TO DO;
                    line = MacroUtils.EMPTY_STRING;
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e) {
                            // Nothing particular to do
                        }
                    }
                }
            } else {
                line = MacroUtils.EMPTY_STRING;
            }
        }
        return line;
    }

}
