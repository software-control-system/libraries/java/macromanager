package fr.soleil.ij.macro.adapter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.ij.macro.util.MacroUtils;
import ij.IJ;
import ij.macro.ExtensionDescriptor;
import ij.macro.Functions;
import ij.macro.MacroExtension;
import ij.plugin.PlugIn;

/**
 * This is a class that is used to allow String[] interaction with ImageJ macros
 * 
 * @author GIRARDOT
 */
public class MacroStringSpectrumAdapter implements IMacroAdapter, PlugIn, MacroExtension {

    protected static final String GET_STRING_VALUE_AT_SELECTED_SPECTRUM_INDEX = "getStringValueAtSelectedSpectrumIndex";
    protected static final String GET_STRING_SPECTRUM_INDEX = "getStringSpectrumIndex";
    protected static final String SET_STRING_SPECTRUM_INDEX = "setStringSpectrumIndex";
    protected static final String GET_STRING_SPECTRUM_KEY = "getStringSpectrumKey";
    protected static final String SET_STRING_SPECTRUM_KEY = "setStringSpectrumKey";
    protected static final String SET_STRING_SPECTRUM = "setStringSpectrum";
    protected static final String GET_STRING_SPECTRUM_LENGTH = "getStringSpectrumLength";
    protected static final String DELETE_STRING_SPECTRUM = "deleteStringSpectrum";

    protected final static Map<String, String> STRING_W_TABLE = new ConcurrentHashMap<>();
    protected final static Map<String, String[]> SPECTRUM_RW_TABLE = new ConcurrentHashMap<>();
    protected static String[] currentSpectrum = null;
    protected static String currentKey = null;
    protected static int currentIndex = 0;

    private static MacroStringSpectrumAdapter adapter = null;

    private ExtensionDescriptor[] extensions = {
            ExtensionDescriptor.newDescriptor(GET_STRING_VALUE_AT_SELECTED_SPECTRUM_INDEX, this,
                    ARG_OUTPUT + ARG_STRING),
            ExtensionDescriptor.newDescriptor(GET_STRING_SPECTRUM_INDEX, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(SET_STRING_SPECTRUM_INDEX, this, ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_STRING_SPECTRUM_KEY, this, ARG_OUTPUT + ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_STRING_SPECTRUM_KEY, this, ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_STRING_SPECTRUM, this, ARG_ARRAY),
            ExtensionDescriptor.newDescriptor(GET_STRING_SPECTRUM_LENGTH, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(DELETE_STRING_SPECTRUM, this) };

    public static void initPluginConnexion() {
        if (adapter == null) {
            adapter = new MacroStringSpectrumAdapter();
        }
    }

    public MacroStringSpectrumAdapter() {
        super();
        try {
            IJ.getClassLoader().loadClass(MacroStringSpectrumAdapter.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method can be called by ImageJ macros, using the "call" method (
     * {@link http://rsbweb.nih.gov/ij/developer/macro/functions.html#C}). It allows the macro to store a String
     * spectrum element by element, with a particular key. The spectrum can then be reconstructed using
     * {@link #computeSpectrum(String, String)}
     * 
     * @param key The key to which to store the spectrum
     * @param index The index of the {@link String} to store
     * @param value The {@link String} value to store.
     * @return {@link #OK_RESULT} if the line was successfully stored, or {@link #KO_RESULT} if there was a problem
     * @see #computeSpectrum(String, String)
     */
    public static String writeString(String key, String index, String value) {
        String result;
        int i = -1;
        try {
            i = Integer.parseInt(index);
        } catch (Exception e) {
            i = -1;
        }
        if ((i > -1) && (key != null) && (value != null)) {
            key = key.trim();
            STRING_W_TABLE.put(key + MacroUtils.KEY_SEPARATOR + i, value);
            key = null;
            value = null;
            result = OK_RESULT;
        } else {
            result = KO_RESULT;
        }
        return result;
    }

    /**
     * Reconstructs an Spectrum previously stored element by element, and cleans the stored elements.
     * 
     * @param key The key with which the elements were stored.
     * @param length The length dimension of the spectrum.
     * @return {@link #OK_RESULT} if the spectrum was successfully reconstructed, or {@link #KO_RESULT} if there was a
     *         problem
     * @see #writeString(String, String, String, String)
     */
    public static String computeSpectrum(String key, String length) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            key = key.trim();
            int size = -1;
            try {
                size = Integer.parseInt(length);
            } catch (Exception e) {
                size = -1;
            }
            if (size < 0) {
                result = KO_RESULT;
            } else {
                String[] spectrum = new String[size];
                for (int i = 0; i < size; i++) {
                    spectrum[i] = STRING_W_TABLE.get(key + MacroUtils.KEY_SEPARATOR + i);
                    STRING_W_TABLE.remove(key + MacroUtils.KEY_SEPARATOR + i);
                }
                SPECTRUM_RW_TABLE.put(key, spectrum);
                result = OK_RESULT;
            }
        }
        return result;
    }

    /**
     * Returns the length of a stored spectrum
     * 
     * @param key The key with which the spectrum was stored
     * @return an int value
     */
    public static int getLength(String key) {
        int result;
        if (key == null) {
            result = 0;
        } else {
            key = key.trim();
            if (SPECTRUM_RW_TABLE.get(key) == null) {
                result = 0;
            } else {
                result = SPECTRUM_RW_TABLE.get(key).length;
            }
        }
        return result;
    }

    /**
     * This method allows an ImageJ macro to get an value in an spectrum you previously stored as a String[], using
     * {@link #setSpectrum(String, String[])}.
     * 
     * @param key The key with which the spectrum was stored
     * @param xIndex The index in x of the {@link String} to check
     * @param yIndex The index in y of the {@link String} to check
     * @return a {@link String}.MacroUtils.EMPTY_STRING if there was a problem.
     * @see #setSpectrum(String, String[])
     */
    public static String getValueAt(String key, String index) {
        String result;
        if (key == null) {
            result = MacroUtils.EMPTY_STRING;
        } else {
            key = key.trim();
            int i = -1;
            try {
                i = Integer.parseInt(index);
            } catch (Exception e) {
                i = -1;
            }
            if ((i > -1) && (key != null)) {
                key = key.trim();
                String[] spectrum = SPECTRUM_RW_TABLE.get(key);
                if (spectrum == null) {
                    result = MacroUtils.EMPTY_STRING;
                } else {
                    if (i < spectrum.length) {
                        if (spectrum[i] == null) {
                            result = MacroUtils.EMPTY_STRING;
                        } else {
                            result = spectrum[i];
                        }
                    } else {
                        result = MacroUtils.EMPTY_STRING;
                    }
                }
            } else {
                result = MacroUtils.EMPTY_STRING;
            }
        }
        return result;
    }

    /**
     * Saves an spectrum that ImageJ's macro can access later using {@link #getValueAt(String, String, String)}
     * 
     * @param key The key to which to store the spectrum
     * @param value the spectrum
     */
    public static void setSpectrum(String key, String[] value) {
        if (key != null) {
            key = key.trim();
            if (value == null) {
                SPECTRUM_RW_TABLE.remove(key);
            } else {
                SPECTRUM_RW_TABLE.put(key, value);
            }
        }
    }

    /**
     * Returns the spectrum stored with the expected index. Generally, you will call this once the ImageJ macro used
     * {@link #computeSpectrum(String, String, String)}. But you can also use this to access to an spectrum you stored
     * with {@link #setSpectrum(String, String[])}
     * 
     * @param key The key with which the spectrum was stored
     * @return The spectrum stored with the expected key.<code>null</code> if no spectrum corresponds to the given key.
     * @see #setSpectrum(String, String[])
     * @see #computeSpectrum(String, String, String)
     */
    public static String[] getSpectrum(String key) {
        String[] result;
        if (key == null) {
            result = null;
        } else {
            result = SPECTRUM_RW_TABLE.get(key.trim());
        }
        return result;
    }

    /**
     * Deletes an spectrum stored with a particular key, in order to free memory space.
     * 
     * @param key The key with which the spectrum was stored
     */
    public static String deleteSpectrum(String key) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            SPECTRUM_RW_TABLE.remove(key.trim());
            result = OK_RESULT;
        }
        return result;
    }

    @Override
    public void run(String arg0) {
        Functions.registerExtensions(this);
    }

    @Override
    public ExtensionDescriptor[] getExtensionFunctions() {
        return extensions;
    }

    @Override
    public String handleExtension(String name, Object[] args) {
        switch (name) {
            case GET_STRING_VALUE_AT_SELECTED_SPECTRUM_INDEX:
                String value;
                if ((currentSpectrum == null) || (currentIndex < 0) || (currentIndex >= currentSpectrum.length)) {
                    value = MacroUtils.EMPTY_STRING;
                } else {
                    value = (currentSpectrum[currentIndex] == null ? MacroUtils.EMPTY_STRING
                            : currentSpectrum[currentIndex]);
                }
                ((String[]) args[0])[0] = value;
                value = null;
                break;
            case GET_STRING_SPECTRUM_INDEX:
                ((Double[]) args[0])[0] = Double.valueOf(currentIndex);
                break;
            case SET_STRING_SPECTRUM_INDEX:
                currentIndex = ((Double) args[0]).intValue();
                break;
            case GET_STRING_SPECTRUM_KEY:
                ((String[]) args[0])[0] = (currentKey == null ? MacroUtils.EMPTY_STRING : currentKey);
                break;
            case SET_STRING_SPECTRUM_KEY:
                currentKey = (String) args[0];
                if (currentKey != null) {
                    currentKey = currentKey.trim();
                }
                currentSpectrum = getSpectrum(currentKey);
                currentIndex = 0;
                break;
            case SET_STRING_SPECTRUM:
                if (currentKey != null) {
                    Object[] temp = (Object[]) args[0];
                    if (temp != null) {
                        String[] spectrum = new String[temp.length];
                        for (int i = 0; i < temp.length; i++) {
                            if (temp[i] instanceof String) {
                                spectrum[i] = (String) temp[i];
                            } else {
                                spectrum[i] = MacroUtils.EMPTY_STRING;
                            }
                        }
                        setSpectrum(currentKey, spectrum);
                        currentSpectrum = spectrum;
                        spectrum = null;
                    }
                    temp = null;
                }
                break;
            case GET_STRING_SPECTRUM_LENGTH:
                int length = 0;
                if (currentSpectrum != null) {
                    length = currentSpectrum.length;
                }
                ((Double[]) args[0])[0] = Double.valueOf(length);
                break;
            case DELETE_STRING_SPECTRUM:
                currentSpectrum = null;
                currentIndex = 0;
                deleteSpectrum(currentKey);
                break;
        }
        return null;
    }

    public static void clean() {
        STRING_W_TABLE.clear();
        SPECTRUM_RW_TABLE.clear();
    }

}
