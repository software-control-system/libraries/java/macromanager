package fr.soleil.ij.macro.adapter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.ij.macro.util.MacroUtils;

public class MacroStringScalarAdapter implements IMacroAdapter {

    protected final static Map<String, String> STRING_RW_TABLE = new ConcurrentHashMap<>();

    public static String setString(String key, String value) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            key = key.trim();
            if (value == null) {
                STRING_RW_TABLE.remove(key);
            } else {
                STRING_RW_TABLE.put(key, value);
            }
            result = OK_RESULT;
        }
        return result;
    }

    public static String deleteString(String key) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            STRING_RW_TABLE.remove(key.trim());
            result = OK_RESULT;
        }
        return result;
    }

    public static String getValue(String key) {
        String result;
        if (key == null) {
            result = MacroUtils.EMPTY_STRING;
        } else {
            key = key.trim();
            String value = STRING_RW_TABLE.get(key);
            if (value == null) {
                result = MacroUtils.EMPTY_STRING;
            } else {
                result = value;
            }
        }
        return result;
    }

    public static String getString(String key) {
        String result;
        if (key == null) {
            result = null;
        } else {
            key = key.trim();
            result = STRING_RW_TABLE.get(key);
        }
        return result;
    }

    public static void clean() {
        STRING_RW_TABLE.clear();
    }

}
