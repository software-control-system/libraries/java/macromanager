package fr.soleil.ij.macro.adapter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.ij.macro.util.MacroUtils;
import ij.IJ;
import ij.macro.ExtensionDescriptor;
import ij.macro.Functions;
import ij.macro.MacroExtension;
import ij.plugin.PlugIn;

public class MacroBooleanSpectrumAdapter implements IMacroAdapter, PlugIn, MacroExtension {

    protected static final String GET_BOOLEAN_VALUE_AT_SELECTED_SPECTRUM_INDEX = "getBooleanValueAtSelectedSpectrumIndex";
    protected static final String GET_BOOLEAN_SPECTRUM_INDEX = "getBooleanSpectrumIndex";
    protected static final String SET_BOOLEAN_SPECTRUM_INDEX = "setBooleanSpectrumIndex";
    protected static final String GET_BOOLEAN_SPECTRUM_KEY = "getBooleanSpectrumKey";
    protected static final String SET_BOOLEAN_SPECTRUM_KEY = "setBooleanSpectrumKey";
    protected static final String SET_BOOLEAN_SPECTRUM = "setBooleanSpectrum";
    protected static final String GET_BOOLEAN_SPECTRUM_LENGTH = "getBooleanSpectrumLength";
    protected static final String DELETE_BOOLEAN_SPECTRUM = "deleteBooleanSpectrum";

    protected final static Map<String, boolean[]> SPECTRUM_RW_TABLE = new ConcurrentHashMap<>();
    protected static boolean[] currentSpectrum = null;
    protected static String currentKey = null;
    protected static int currentIndex = 0;

    private static MacroBooleanSpectrumAdapter adapter = null;

    private ExtensionDescriptor[] extensions = {
            ExtensionDescriptor.newDescriptor(GET_BOOLEAN_VALUE_AT_SELECTED_SPECTRUM_INDEX, this,
                    ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_BOOLEAN_SPECTRUM_INDEX, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(SET_BOOLEAN_SPECTRUM_INDEX, this, ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_BOOLEAN_SPECTRUM_KEY, this, ARG_OUTPUT + ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_BOOLEAN_SPECTRUM_KEY, this, ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_BOOLEAN_SPECTRUM, this, ARG_ARRAY),
            ExtensionDescriptor.newDescriptor(GET_BOOLEAN_SPECTRUM_LENGTH, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(DELETE_BOOLEAN_SPECTRUM, this) };

    public static void initPluginConnexion() {
        if (adapter == null) {
            adapter = new MacroBooleanSpectrumAdapter();
        }
    }

    public MacroBooleanSpectrumAdapter() {
        super();
        try {
            IJ.getClassLoader().loadClass(MacroBooleanSpectrumAdapter.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String writeSpectrum(String key, String value) {
        String result;
        if (key == null || value == null) {
            result = KO_RESULT;
        } else {
            key = key.trim();
            String[] stringArray = value.split(MacroUtils.SEMICOLON);
            boolean[] spectrum = new boolean[stringArray.length];
            for (int i = 0; i < stringArray.length; i++) {
                try {
                    spectrum[i] = Boolean.parseBoolean(stringArray[i]);
                } catch (Exception e) {
                    spectrum[i] = false;
                }
            }
            result = OK_RESULT;
        }
        return result;
    }

    public static void setSpectrum(String key, boolean[] value) {
        if (key != null) {
            key = key.trim();
            if (value == null) {
                SPECTRUM_RW_TABLE.remove(key);
            } else {
                SPECTRUM_RW_TABLE.put(key, value);
            }
        }
    }

    public static String deleteSpectrum(String key) {
        String result = KO_RESULT;
        if (key != null) {
            SPECTRUM_RW_TABLE.remove(key.trim());
            result = OK_RESULT;
        }
        return result;
    }

    public static boolean[] getSpectrum(String key) {
        boolean[] result = null;
        if (key != null) {
            key = key.trim();
            result = SPECTRUM_RW_TABLE.get(key);
        }
        return result;
    }

    public static String getSpectrumAsString(String key) {
        StringBuilder resultBuffer = new StringBuilder();
        if (key != null) {
            key = key.trim();
            boolean[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                for (int i = 0; i < spectrum.length; i++) {
                    if (i > 0) {
                        resultBuffer.append(MacroUtils.SEMICOLON);
                    }
                    resultBuffer.append(Boolean.toString(spectrum[i]));
                }
            }
            spectrum = null;
        }
        return resultBuffer.toString();
    }

    public static int getLength(String key) {
        int result = 0;
        if (key != null) {
            key = key.trim();
            boolean[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                result = spectrum.length;
                spectrum = null;
            }
        }
        return result;
    }

//    public static String getMaximum(String key) {
//        String result =MacroUtils.EMPTY_STRING;
//        if (key != null) {
//            key = key.trim();
//            boolean[] spectrum = SPECTRUM_RW_TABLE.get(key);
//            if (spectrum != null) {
//                if (spectrum.length > 0) {
//                    boolean max = Double.NEGATIVE_INFINITY;
//                    for (int i = 0; i < spectrum.length; i++) {
//                        if (spectrum[i] > max) {
//                            max = spectrum[i];
//                        }
//                    }
//                }
//                spectrum = null;
//            }
//        }
//        return result;
//    }
//
//    public static String getMinimum(String key) {
//        String result =MacroUtils.EMPTY_STRING;
//        if (key != null) {
//            key = key.trim();
//            boolean[] spectrum = SPECTRUM_RW_TABLE.get(key);
//            if (spectrum != null) {
//                if (spectrum.length > 0) {
//                    boolean min = Double.POSITIVE_INFINITY;
//                    for (int i = 0; i < spectrum.length; i++) {
//                        if (spectrum[i] < min) {
//                            min = spectrum[i];
//                        }
//                    }
//                }
//                spectrum = null;
//            }
//        }
//        return result;
//    }
//
//    public static boolean getMean(String key) {
//        boolean result = Double.NaN;
//        if (key != null) {
//            key = key.trim();
//            boolean[] spectrum = SPECTRUM_RW_TABLE.get(key);
//            if (spectrum != null) {
//                if (spectrum.length > 0) {
//                    boolean cumul = 0;
//                    for (int i = 0; i < spectrum.length; i++) {
//                        cumul += spectrum[i];
//                    }
//                    result = cumul / spectrum.length;
//                }
//                spectrum = null;
//            }
//        }
//        return result;
//    }

    public static String getValueAt(String key, String index) {
        String result = MacroUtils.EMPTY_STRING;
        if (key != null) {
            int i = -1;
            boolean[] spectrum = SPECTRUM_RW_TABLE.get(key);
            if (spectrum != null) {
                try {
                    i = Integer.parseInt(index);
                } catch (Exception e) {
                    i = -1;
                }
                if (i > -1 && i < spectrum.length) {
                    result = result + spectrum[i];
                }
            }
        }
        return result;
    }

    @Override
    public void run(String arg0) {
        Functions.registerExtensions(this);
    }

    @Override
    public ExtensionDescriptor[] getExtensionFunctions() {
        return extensions;
    }

    @Override
    public String handleExtension(String name, Object[] args) {
        switch (name) {
            case GET_BOOLEAN_VALUE_AT_SELECTED_SPECTRUM_INDEX:
                boolean value;
                if ((currentSpectrum == null) || (currentIndex < 0) || (currentIndex >= currentSpectrum.length)) {
                    value = false;
                } else {
                    value = currentSpectrum[currentIndex];
                }
                ((Double[]) args[0])[0] = MacroUtils.toDouble(value);
                break;
            case GET_BOOLEAN_SPECTRUM_INDEX:
                ((Double[]) args[0])[0] = Double.valueOf(currentIndex);
                break;
            case SET_BOOLEAN_SPECTRUM_INDEX:
                currentIndex = ((Double) args[0]).intValue();
                break;
            case GET_BOOLEAN_SPECTRUM_KEY:
                ((String[]) args[0])[0] = (currentKey == null ? MacroUtils.EMPTY_STRING : currentKey);
                break;
            case SET_BOOLEAN_SPECTRUM_KEY:
                currentKey = (String) args[0];
                if (currentKey != null) {
                    currentKey = currentKey.trim();
                }
                currentSpectrum = getSpectrum(currentKey);
                currentIndex = 0;
                break;
            case SET_BOOLEAN_SPECTRUM:
                if (currentKey != null) {
                    Object[] temp = (Object[]) args[0];
                    if (temp != null) {
                        boolean[] spectrum = new boolean[temp.length];
                        for (int i = 0; i < temp.length; i++) {
                            spectrum[i] = MacroUtils.toBooleanFromNbObject(temp[i]);
                        }
                        setSpectrum(currentKey, spectrum);
                        currentSpectrum = spectrum;
                    }
                }
                break;
            case GET_BOOLEAN_SPECTRUM_LENGTH:
                int length = 0;
                if (currentSpectrum != null) {
                    length = currentSpectrum.length;
                }
                ((Double[]) args[0])[0] = Double.valueOf(length);
                break;
            case DELETE_BOOLEAN_SPECTRUM:
                currentSpectrum = null;
                currentIndex = 0;
                deleteSpectrum(currentKey);
                break;
        }
        return null;
    }

    public static void clean() {
        SPECTRUM_RW_TABLE.clear();
    }

}
