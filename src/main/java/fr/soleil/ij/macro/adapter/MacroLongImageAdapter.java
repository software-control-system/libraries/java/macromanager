package fr.soleil.ij.macro.adapter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.ij.macro.util.MacroUtils;
import ij.IJ;
import ij.macro.ExtensionDescriptor;
import ij.macro.Functions;
import ij.macro.MacroExtension;
import ij.plugin.PlugIn;

/**
 * This is a class that is used to allow long[][] interaction with ImageJ macros
 * 
 * @author GIRARDOT
 */
public class MacroLongImageAdapter implements IMacroAdapter, PlugIn, MacroExtension {

    protected static final String GET_LONG_VALUE_AT_SELECTED_IMAGE_XY = "getLongValueAtSelectedImageXY";
    protected static final String GET_LONG_IMAGE_X = "getLongImageX";
    protected static final String SET_LONG_IMAGE_X = "setLongImageX";
    protected static final String GET_LONG_IMAGE_Y = "getLongImageY";
    protected static final String SET_LONG_IMAGE_Y = "setLongImageY";
    protected static final String GET_LONG_IMAGE_KEY = "getLongImageKey";
    protected static final String SET_LONG_IMAGE_KEY = "setLongImageKey";
    protected static final String SET_LINE_AT_SELECTED_LONG_IMAGE_Y = "setLineAtSelectedLongImageY";
    protected static final String SET_LONG_IMAGE_SIZE = "setLongImageSize";
    protected static final String GET_LONG_IMAGE_WIDTH = "getLongImageWidth";
    protected static final String GET_LONG_IMAGE_HEIGHT = "getLongImageHeight";
    protected static final String DELETE_LONG_IMAGE = "deleteLongImage";

    protected final static Map<String, long[]> LINE_W_TABLE = new ConcurrentHashMap<>();
    protected final static Map<String, long[][]> IMAGE_RW_TABLE = new ConcurrentHashMap<>();
    protected static long[][] currentImage = null;
    protected static String currentKey = null;
    protected static int currentX = 0;
    protected static int currentY = 0;

    private static MacroLongImageAdapter adapter = null;

    private ExtensionDescriptor[] extensions = {
            ExtensionDescriptor.newDescriptor(GET_LONG_VALUE_AT_SELECTED_IMAGE_XY, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_LONG_IMAGE_X, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(SET_LONG_IMAGE_X, this, ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_LONG_IMAGE_Y, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(SET_LONG_IMAGE_Y, this, ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_LONG_IMAGE_KEY, this, ARG_OUTPUT + ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_LONG_IMAGE_KEY, this, ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_LINE_AT_SELECTED_LONG_IMAGE_Y, this, ARG_ARRAY),
            ExtensionDescriptor.newDescriptor(SET_LONG_IMAGE_SIZE, this, ARG_ARRAY),
            ExtensionDescriptor.newDescriptor(GET_LONG_IMAGE_WIDTH, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_LONG_IMAGE_HEIGHT, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(DELETE_LONG_IMAGE, this) };

    public static void initPluginConnexion() {
        if (adapter == null) {
            adapter = new MacroLongImageAdapter();
        }
    }

    public MacroLongImageAdapter() {
        super();
        try {
            IJ.getClassLoader().loadClass(MacroLongImageAdapter.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method can be called by ImageJ macros, using the "call" method (
     * {@link http://rsbweb.nih.gov/ij/developer/macro/functions.html#C}). It allows the macro to store an image line by
     * line, with a particular key. {@link #computeImage(String, String)}
     * 
     * @param key The key to which to store the image
     * @param index The index of the line to store
     * @param line The line. This line is then transformed into a long[]. The line is expected to be formated that way :
     *            "value1;value2;...;valuen", each value being possibly parsed as a long.
     * @return {@link #OK_RESULT} if the line was successfully stored, or {@link #KO_RESULT} if there was a problem
     * @see #computeImage(String, String)
     */
    public static String writeLine(String key, String index, String line) {
        String result;
        int lineIndex = -1;
        try {
            lineIndex = Integer.parseInt(index);
        } catch (Exception e) {
            lineIndex = -1;
        }
        if ((lineIndex > -1) && (key != null) && (line != null)) {
            key = key.trim();
            String[] splited = line.split(MacroUtils.SEMICOLON);
            long[] toStore = new long[splited.length];
            for (int i = 0; i < splited.length; i++) {
                try {
                    toStore[i] = Long.parseLong(splited[i]);
                } catch (Exception e) {
                    toStore[i] = 0;
                }
            }
            splited = null;
            LINE_W_TABLE.put(key + index, toStore);
            toStore = null;
            key = null;
            line = null;
            result = OK_RESULT;
        } else {
            result = KO_RESULT;
        }
        return result;
    }

    /**
     * Reconstructs an Image previously stored line by line, and cleans the stored lines.
     * 
     * @param key The key with which the lines were stored.
     * @param lineCount The number of lines previously stored. This will then match the image Y dimension.
     * @return {@link #OK_RESULT} if the image was successfully reconstructed, or {@link #KO_RESULT} if there was a
     *         problem
     * @see #writeLine(String, String, String)
     */
    public static String computeImage(String key, String lineCount) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            key = key.trim();
            int count = -1;
            try {
                count = Integer.parseInt(lineCount);
            } catch (Exception e) {
                count = -1;
            }
            if (count < 0) {
                result = KO_RESULT;
            } else {
                int firstValidIndex = -1;
                for (int i = 0; i < count; i++) {
                    if (LINE_W_TABLE.get(key + i) != null) {
                        firstValidIndex = i;
                        break;
                    }
                }
                long[][] image = null;
                if (firstValidIndex > -1) {
                    image = new long[count][LINE_W_TABLE.get(key + firstValidIndex).length];
                    for (int i = 0; i < count; i++) {
                        String lineKey = key + i;
                        if (LINE_W_TABLE.get(lineKey) == null) {
                            for (int j = 0; j < image[i].length; j++) {
                                image[i][j] = 0;
                            }
                        } else if (LINE_W_TABLE.get(lineKey).length == image[i].length) {
                            image[i] = LINE_W_TABLE.get(lineKey);
                        } else {
                            long[] temp = LINE_W_TABLE.get(lineKey);
                            int min = Math.min(image[i].length, temp.length);
                            for (int j = 0; j < min; j++) {
                                image[i][j] = temp[j];
                            }
                            for (int j = min; j < image[i].length; j++) {
                                image[i][j] = 0;
                            }
                            temp = null;
                        }
                        LINE_W_TABLE.remove(lineKey);
                        lineKey = null;
                    }
                }
                if (image == null) {
                    IMAGE_RW_TABLE.remove(key);
                } else {
                    IMAGE_RW_TABLE.put(key, image);
                }
                result = OK_RESULT;
            }
        }
        return result;
    }

    /**
     * Returns the Y dimension of a stored image
     * 
     * @param key The key with which the image was stored
     * @return an int value
     */
    public static int getDimY(String key) {
        int result;
        if (key == null) {
            result = 0;
        } else {
            key = key.trim();
            if (IMAGE_RW_TABLE.get(key) == null) {
                result = 0;
            } else {
                result = IMAGE_RW_TABLE.get(key).length;
            }
        }
        return result;
    }

    /**
     * Returns the X dimension of a stored image
     * 
     * @param key The key with which the image was stored
     * @return an int value
     */
    public static int getDimX(String key) {
        int result;
        if (getDimY(key) == 0) {
            result = 0;
        } else {
            result = IMAGE_RW_TABLE.get(key.trim())[0].length;
        }
        return result;
    }

    /**
     * This method allows an ImageJ macro to read line by line (row by row) an image you previously stored as a
     * long[][], using {@link #setImage(String, long[][])}.
     * 
     * @param key The key with which the image was stored
     * @param index the line index
     * @return The {@link String} representation of the row/line
     * @see #setImage(String, long[][])
     */
    public static String getLine(String key, String index) {
        String result;
        if (key == null) {
            result = MacroUtils.EMPTY_STRING;
        } else {
            key = key.trim();
            int lineIndex = -1;
            try {
                lineIndex = Integer.parseInt(index);
            } catch (Exception e) {
                lineIndex = -1;
            }
            if (lineIndex < 0) {
                result = MacroUtils.EMPTY_STRING;
            } else {
                StringBuilder buffer = new StringBuilder();
                if (IMAGE_RW_TABLE.get(key) != null && lineIndex < IMAGE_RW_TABLE.get(key).length) {
                    long[] temp = IMAGE_RW_TABLE.get(key)[lineIndex];
                    buffer.append(Long.toString(temp[0]));
                    for (int i = 1; i < temp.length; i++) {
                        buffer.append(MacroUtils.SEMICOLON).append(Long.toString(temp[i]));
                    }
                    temp = null;
                }
                result = buffer.toString();
            }
        }
        return result;
    }

    /**
     * This method allows an ImageJ macro to get an value in an image you previously stored as a long[][], using
     * {@link #setImage(String, long[][])}
     * 
     * @param key The key with which the image was stored
     * @param xIndex The index in x of the value to check
     * @param yIndex The index in y of the value to check
     * @return The {@link String} representation of the value at the specified indexes.MacroUtils.EMPTY_STRING if there
     *         was a problem.
     * @see #setImage(String, long[][])
     */
    public static String getValueAt(String key, String xIndex, String yIndex) {
        String result;
        if (key == null) {
            result = MacroUtils.EMPTY_STRING;
        } else {
            key = key.trim();
            int x = -1, y = -1;
            try {
                x = Integer.parseInt(xIndex);
                y = Integer.parseInt(yIndex);
            } catch (Exception e) {
                x = -1;
                y = -1;
            }
            if ((x > -1) && (y > -1) && (key != null)) {
                key = key.trim();
                long[][] image = IMAGE_RW_TABLE.get(key);
                if (image == null) {
                    result = MacroUtils.EMPTY_STRING;
                } else {
                    if (y < image.length && x < image[y].length) {
                        result = Long.toString(image[y][x]);
                    } else {
                        result = MacroUtils.EMPTY_STRING;
                    }
                }
            } else {
                result = MacroUtils.EMPTY_STRING;
            }
        }
        return result;
    }

    /**
     * Saves an image that ImageJ's macro can access later using {@link #getLine(String, String)}
     * 
     * @param key The key to which to store the image
     * @param value the image
     */
    public static void setImage(String key, long[][] value) {
        if (key != null) {
            key = key.trim();
            if (value == null) {
                IMAGE_RW_TABLE.remove(key);
            } else {
                IMAGE_RW_TABLE.put(key, value);
            }
        }
    }

    /**
     * Returns the image stored with the expected index. Generally, you will call this once the ImageJ macro used
     * {@link #computeImage(String, String)}. But you can also use this to access to an image you stored with
     * {@link #setImage(String, long[][])}
     * 
     * @param key The key with which the image was stored
     * @return The image stored with the expected key.<code>null</code> if no image corresponds to the given key.
     * @see #setImage(String, long[][])
     * @see #computeImage(String, String)
     */
    public static long[][] getImage(String key) {
        long[][] result;
        if (key == null) {
            result = null;
        } else {
            result = IMAGE_RW_TABLE.get(key.trim());
        }
        return result;
    }

    /**
     * Deletes an image stored with a particular key, in order to free memory space.
     * 
     * @param key The key with which the image was stored
     */
    public static String deleteImage(String key) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            IMAGE_RW_TABLE.remove(key.trim());
            result = OK_RESULT;
        }
        return result;
    }

    @Override
    public void run(String arg0) {
        Functions.registerExtensions(this);
    }

    @Override
    public ExtensionDescriptor[] getExtensionFunctions() {
        return extensions;
    }

    @Override
    public String handleExtension(String name, Object[] args) {
        switch (name) {
            case GET_LONG_VALUE_AT_SELECTED_IMAGE_XY:
                Double value;
                if ((currentImage == null) || (currentY < 0) || (currentY >= currentImage.length) || (currentX < 0)
                        || (currentX >= currentImage[currentY].length)) {
                    value = Double.valueOf(Double.NaN);
                } else {
                    value = Double.valueOf(currentImage[currentY][currentX]);
                }
                ((Double[]) args[0])[0] = value;
                break;
            case GET_LONG_IMAGE_X:
                ((Double[]) args[0])[0] = Double.valueOf(currentX);
                break;
            case SET_LONG_IMAGE_X:
                currentX = ((Double) args[0]).intValue();
                break;
            case GET_LONG_IMAGE_Y:
                ((Double[]) args[0])[0] = Double.valueOf(currentY);
                break;
            case SET_LONG_IMAGE_Y:
                currentY = ((Double) args[0]).intValue();
                break;
            case GET_LONG_IMAGE_KEY:
                ((String[]) args[0])[0] = (currentKey == null ? MacroUtils.EMPTY_STRING : currentKey);
                break;
            case SET_LONG_IMAGE_KEY:
                currentKey = (String) args[0];
                if (currentKey != null) {
                    currentKey = currentKey.trim();
                }
                currentImage = getImage(currentKey);
                currentX = 0;
                currentY = 0;
                break;
            case SET_LINE_AT_SELECTED_LONG_IMAGE_Y:
                if ((currentKey != null) && (currentImage != null) && (currentY > -1)
                        && (currentY < currentImage.length)) {
                    Object[] temp = (Object[]) args[0];
                    if ((temp != null) && (temp.length == currentImage[currentY].length)) {
                        long[] line = new long[temp.length];
                        for (int i = 0; i < temp.length; i++) {
                            if (temp[i] instanceof Double) {
                                line[i] = ((Double) temp[i]).longValue();
                            } else {
                                line[i] = 0;
                            }
                        }
                        currentImage[currentY] = line;
                        setImage(currentKey, currentImage);
                    }
                }
                break;
            case SET_LONG_IMAGE_SIZE:
                Object[] temp = (Object[]) args[0];
                if ((temp != null) && (temp.length == 2)) {
                    int width = 0, height = 0;
                    if (temp[0] instanceof Double) {
                        width = ((Double) temp[0]).intValue();
                        if (width < 0) {
                            width = 0;
                        }
                    }
                    if (temp[1] instanceof Double) {
                        height = ((Double) temp[1]).intValue();
                        if (height < 0) {
                            height = 0;
                        }
                    }
                    currentImage = new long[height][width];
                }
                break;
            case GET_LONG_IMAGE_WIDTH:
                int length = 0;
                if ((currentImage != null) && (currentImage.length > 0)) {
                    length = currentImage[0].length;
                }
                ((Double[]) args[0])[0] = Double.valueOf(length);
                break;
            case GET_LONG_IMAGE_HEIGHT:
                length = 0;
                if (currentImage != null) {
                    length = currentImage.length;
                }
                ((Double[]) args[0])[0] = Double.valueOf(length);
                break;
            case DELETE_LONG_IMAGE:
                currentImage = null;
                currentX = 0;
                currentY = 0;
                deleteImage(currentKey);
                break;
        }
        return null;
    }

    public static void clean() {
        LINE_W_TABLE.clear();
        IMAGE_RW_TABLE.clear();
    }

}
