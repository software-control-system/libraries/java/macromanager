package fr.soleil.ij.macro.adapter;

public interface IMacroAdapter {

    /**
     * A {@link String} used to say that a method successfully ran
     */
    public final static String OK_RESULT = "OK";
    /**
     * A {@link String} used to say that a method faced a problem while running
     */
    public final static String KO_RESULT = "KO";

}
