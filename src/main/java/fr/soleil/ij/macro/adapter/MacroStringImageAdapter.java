package fr.soleil.ij.macro.adapter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.ij.macro.util.MacroUtils;
import ij.IJ;
import ij.macro.ExtensionDescriptor;
import ij.macro.Functions;
import ij.macro.MacroExtension;
import ij.plugin.PlugIn;

/**
 * This is a class that is used to allow String[][] interaction with ImageJ
 * macros
 * 
 * @author GIRARDOT
 */
public class MacroStringImageAdapter implements IMacroAdapter, PlugIn, MacroExtension {

    protected static final String GET_STRING_VALUE_AT_SELECTED_IMAGE_XY = "getStringValueAtSelectedImageXY";
    protected static final String GET_STRING_IMAGE_X = "getStringImageX";
    protected static final String SET_STRING_IMAGE_X = "setStringImageX";
    protected static final String GET_STRING_IMAGE_Y = "getStringImageY";
    protected static final String SET_STRING_IMAGE_Y = "setStringImageY";
    protected static final String GET_STRING_IMAGE_KEY = "getStringImageKey";
    protected static final String SET_STRING_IMAGE_KEY = "setStringImageKey";
    protected static final String SET_LINE_AT_SELECTED_STRING_IMAGE_Y = "setLineAtSelectedStringImageY";
    protected static final String SET_STRING_IMAGE_SIZE = "setStringImageSize";
    protected static final String GET_STRING_IMAGE_WIDTH = "getStringImageWidth";
    protected static final String GET_STRING_IMAGE_HEIGHT = "getStringImageHeight";
    protected static final String DELETE_STRING_IMAGE = "deleteStringImage";

    protected final static Map<String, String> STRING_W_TABLE = new ConcurrentHashMap<>();
    protected final static Map<String, String[][]> IMAGE_RW_TABLE = new ConcurrentHashMap<>();
    protected static String[][] currentImage = null;
    protected static String currentKey = null;
    protected static int currentX = 0;
    protected static int currentY = 0;

    private static MacroStringImageAdapter adapter = null;

    private ExtensionDescriptor[] extensions = {
            ExtensionDescriptor.newDescriptor(GET_STRING_VALUE_AT_SELECTED_IMAGE_XY, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_STRING_IMAGE_X, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(SET_STRING_IMAGE_X, this, ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_STRING_IMAGE_Y, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(SET_STRING_IMAGE_Y, this, ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_STRING_IMAGE_KEY, this, ARG_OUTPUT + ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_STRING_IMAGE_KEY, this, ARG_STRING),
            ExtensionDescriptor.newDescriptor(SET_LINE_AT_SELECTED_STRING_IMAGE_Y, this, ARG_ARRAY),
            ExtensionDescriptor.newDescriptor(SET_STRING_IMAGE_SIZE, this, ARG_ARRAY),
            ExtensionDescriptor.newDescriptor(GET_STRING_IMAGE_WIDTH, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(GET_STRING_IMAGE_HEIGHT, this, ARG_OUTPUT + ARG_NUMBER),
            ExtensionDescriptor.newDescriptor(DELETE_STRING_IMAGE, this) };

    public static void initPluginConnexion() {
        if (adapter == null) {
            adapter = new MacroStringImageAdapter();
        }
    }

    public MacroStringImageAdapter() {
        super();
        try {
            IJ.getClassLoader().loadClass(MacroStringImageAdapter.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method can be called by ImageJ macros, using the "call" method (
     * {@link http://rsbweb.nih.gov/ij/developer/macro/functions.html#C}). It allows the macro to store a String image
     * element by element, with a particular key. The image can then be reconstructed using
     * {@link #computeImage(String, String, String)}
     * 
     * @param key The key to which to store the image
     * @param xIndex The index in x of the {@link String} to store
     * @param yIndex The index in y of the {@link String} to store
     * @param value The {@link String} value to store.
     * @return {@link #OK_RESULT} if the line was successfully stored, or {@link #KO_RESULT} if there was a problem
     * @see #computeImage(String, String, String)
     */
    public static String writeString(String key, String xIndex, String yIndex, String value) {
        String result;
        int x = -1, y = -1;
        try {
            x = Integer.parseInt(xIndex);
            y = Integer.parseInt(yIndex);
        } catch (Exception e) {
            x = -1;
            y = -1;
        }
        if ((x > -1) && (y > -1) && (key != null) && (value != null)) {
            key = key.trim();
            STRING_W_TABLE.put(key + MacroUtils.KEY_SEPARATOR + y + MacroUtils.KEY_SEPARATOR + x, value);
            key = null;
            value = null;
            result = OK_RESULT;
        } else {
            result = KO_RESULT;
        }
        return result;
    }

    /**
     * Reconstructs an Image previously stored element by element, and cleans the stored elements.
     * 
     * @param key The key with which the elements were stored.
     * @param dimX The x dimension of the image.
     * @param dimY The y dimension of the image.
     * @return {@link #OK_RESULT} if the image was successfully reconstructed, or {@link #KO_RESULT} if there was a
     *         problem
     * @see #writeString(String, String, String, String)
     */
    public static String computeImage(String key, String dimX, String dimY) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            key = key.trim();
            int height = -1, width = -1;
            try {
                height = Integer.parseInt(dimY);
                width = Integer.parseInt(dimX);
            } catch (Exception e) {
                height = -1;
                width = -1;
            }
            if (height < 0 || width < 0) {
                result = KO_RESULT;
            } else {
                String[][] image = new String[height][width];
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        String currentKey = key + MacroUtils.KEY_SEPARATOR + i + MacroUtils.KEY_SEPARATOR + j;
                        image[i][j] = STRING_W_TABLE.get(currentKey);
                        STRING_W_TABLE.remove(currentKey);
                        currentKey = null;
                    }
                }
                IMAGE_RW_TABLE.put(key, image);
                result = OK_RESULT;
            }
        }
        return result;
    }

    /**
     * Returns the Y dimension of a stored image
     * 
     * @param key The key with which the image was stored
     * @return an int value
     */
    public static int getDimY(String key) {
        int result;
        if (key == null) {
            result = 0;
        } else {
            key = key.trim();
            if (IMAGE_RW_TABLE.get(key) == null) {
                result = 0;
            } else {
                result = IMAGE_RW_TABLE.get(key).length;
            }
        }
        return result;
    }

    /**
     * Returns the X dimension of a stored image
     * 
     * @param key The key with which the image was stored
     * @return an int value
     */
    public static int getDimX(String key) {
        int result;
        if (getDimY(key) == 0) {
            result = 0;
        } else {
            result = IMAGE_RW_TABLE.get(key.trim())[0].length;
        }
        return result;
    }

    /**
     * This method allows an ImageJ macro to get an value in an image you previously stored as a String[][], using
     * {@link #setImage(String, String[][])}.
     * 
     * @param key The key with which the image was stored
     * @param xIndex The index in x of the {@link String} to check
     * @param yIndex The index in y of the {@link String} to check
     * @return a {@link String}.MacroUtils.EMPTY_STRING if there was a problem.
     * @see #setImage(String, String[][])
     */
    public static String getValueAt(String key, String xIndex, String yIndex) {
        String result;
        if (key == null) {
            result = MacroUtils.EMPTY_STRING;
        } else {
            key = key.trim();
            int x = -1, y = -1;
            try {
                x = Integer.parseInt(xIndex);
                y = Integer.parseInt(yIndex);
            } catch (Exception e) {
                x = -1;
                y = -1;
            }
            if ((x > -1) && (y > -1) && (key != null)) {
                String[][] image = IMAGE_RW_TABLE.get(key);
                if (image == null) {
                    result = MacroUtils.EMPTY_STRING;
                } else {
                    if (y < image.length && x < image[y].length) {
                        if (image[y][x] == null) {
                            result = MacroUtils.EMPTY_STRING;
                        } else {
                            result = image[y][x];
                        }
                    } else {
                        result = MacroUtils.EMPTY_STRING;
                    }
                }
            } else {
                result = MacroUtils.EMPTY_STRING;
            }
        }
        return result;
    }

    /**
     * Saves an image that ImageJ's macro can access later using {@link #getValueAt(String, String, String)}
     * 
     * @param key The key to which to store the image
     * @param value the image
     */
    public static void setImage(String key, String[][] value) {
        if (key != null) {
            key = key.trim();
            if (value == null) {
                IMAGE_RW_TABLE.remove(key);
            } else {
                IMAGE_RW_TABLE.put(key, value);
            }
        }
    }

    /**
     * Returns the image stored with the expected index. Generally, you will call this once the ImageJ macro used
     * {@link #computeImage(String, String, String)}. But you can also use this to access to an image you stored with
     * {@link #setImage(String, String[][])}
     * 
     * @param key The key with which the image was stored
     * @return The image stored with the expected key.<code>null</code> if no image corresponds to the given key.
     * @see #setImage(String, String[][])
     * @see #computeImage(String, String, String)
     */
    public static String[][] getImage(String key) {
        String[][] result;
        if (key == null) {
            result = null;
        } else {
            result = IMAGE_RW_TABLE.get(key.trim());
        }
        return result;
    }

    /**
     * Deletes an image stored with a particular key, in order to free memory space.
     * 
     * @param key The key with which the image was stored
     */
    public static String deleteImage(String key) {
        String result;
        if (key == null) {
            result = KO_RESULT;
        } else {
            IMAGE_RW_TABLE.remove(key.trim());
            result = OK_RESULT;
        }
        return result;
    }

    @Override
    public void run(String arg0) {
        Functions.registerExtensions(this);
    }

    @Override
    public ExtensionDescriptor[] getExtensionFunctions() {
        return extensions;
    }

    @Override
    public String handleExtension(String name, Object[] args) {
        switch (name) {
            case GET_STRING_VALUE_AT_SELECTED_IMAGE_XY:
                String value;
                if ((currentImage == null) || (currentY < 0) || (currentY >= currentImage.length) || (currentX < 0)
                        || (currentX >= currentImage[currentY].length)) {
                    value = MacroUtils.EMPTY_STRING;
                } else {
                    value = (currentImage[currentY][currentX] == null ? MacroUtils.EMPTY_STRING
                            : currentImage[currentY][currentX]);
                }
                ((String[]) args[0])[0] = value;
                value = null;
                break;
            case GET_STRING_IMAGE_X:
                ((Double[]) args[0])[0] = Double.valueOf(currentX);
                break;
            case SET_STRING_IMAGE_X:
                currentX = ((Double) args[0]).intValue();
                break;
            case GET_STRING_IMAGE_Y:
                ((Double[]) args[0])[0] = Double.valueOf(currentY);
                break;
            case SET_STRING_IMAGE_Y:
                currentY = ((Double) args[0]).intValue();
                break;
            case GET_STRING_IMAGE_KEY:
                ((String[]) args[0])[0] = (currentKey == null ? MacroUtils.EMPTY_STRING : currentKey);
                break;
            case SET_STRING_IMAGE_KEY:
                currentKey = (String) args[0];
                if (currentKey != null) {
                    currentKey = currentKey.trim();
                }
                currentImage = getImage(currentKey);
                currentX = 0;
                currentY = 0;
                break;
            case SET_LINE_AT_SELECTED_STRING_IMAGE_Y:
                if ((currentKey != null) && (currentImage != null) && (currentY > -1)
                        && (currentY < currentImage.length)) {
                    Object[] temp = (Object[]) args[0];
                    if ((temp != null) && (temp.length == currentImage[currentY].length)) {
                        String[] line = new String[temp.length];
                        for (int i = 0; i < temp.length; i++) {
                            if (temp[i] instanceof String) {
                                line[i] = (String) temp[i];
                            } else {
                                line[i] = MacroUtils.EMPTY_STRING;
                            }
                        }
                        currentImage[currentY] = line;
                        setImage(currentKey, currentImage);
                        line = null;
                    }
                    temp = null;
                }
                break;
            case SET_STRING_IMAGE_SIZE:
                Object[] temp = (Object[]) args[0];
                if ((temp != null) && (temp.length == 2)) {
                    int width = 0, height = 0;
                    if (temp[0] instanceof Double) {
                        width = ((Double) temp[0]).intValue();
                        if (width < 0) {
                            width = 0;
                        }
                    }
                    if (temp[1] instanceof Double) {
                        height = ((Double) temp[1]).intValue();
                        if (height < 0) {
                            height = 0;
                        }
                    }
                    currentImage = new String[height][width];
                }
                break;
            case GET_STRING_IMAGE_WIDTH:
                int length = 0;
                if ((currentImage != null) && (currentImage.length > 0)) {
                    length = currentImage[0].length;
                }
                ((Double[]) args[0])[0] = Double.valueOf(length);
                break;
            case GET_STRING_IMAGE_HEIGHT:
                length = 0;
                if (currentImage != null) {
                    length = currentImage.length;
                }
                ((Double[]) args[0])[0] = Double.valueOf(length);
                break;
            case DELETE_STRING_IMAGE:
                currentImage = null;
                currentX = 0;
                currentY = 0;
                deleteImage(currentKey);
                break;
        }
        return null;
    }

    public static void clean() {
        STRING_W_TABLE.clear();
        IMAGE_RW_TABLE.clear();
    }

}
