package fr.soleil.ij.macro.header;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.ij.macro.exception.MacroHeaderException;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * A class that can analyze an ImageJ macro to extract its head, according to
 * SOLEIL macro header language specifications.
 * 
 * @author GIRARDOT
 */
public class MacroHeaderExtractor implements Cloneable {

    private String lastMacro;
    private final Map<String, String> header;

    public MacroHeaderExtractor() {
        super();
        lastMacro = null;
        header = new ConcurrentHashMap<>();
    }

    /**
     * Returns the last analyzed macro that was not <code>null</code> or empty
     * 
     * @return a {@link String}
     */
    public String getLastMacro() {
        return lastMacro;
    }

    /**
     * Returns the extracted keys of the last analyzed macro
     * 
     * @return an {@link ArrayList}
     */
    public Collection<String> getKeys() {
        return new ArrayList<String>(header.keySet());
    }

    /**
     * Returns the value associated to a key in the last analyzed macro
     * 
     * @param key the key
     * @return a {@link String}
     */
    public String getValueFor(String key) {
        return header.get(key);
    }

    /**
     * Analyzes an ImageJ macro file to extract its header keys and values. It also stores the corresponding macro in
     * memory.
     * 
     * @param macroPath the path of the macro file to analyze. If the file is not valid (the path is null or empty, or
     *            an exception is thrown during file text extracting), it will have no effect on this
     *            MacroHeaderExtractor. For the rest, it is the same as {@link #analyzeMacro(String)}
     * @return a boolean value.
     *         <ul>
     *         <li><code>true</code> if the macro was successfully analyzed, which means if it exists and its header was
     *         present and correctly formated</li>
     *         <li><code>false</code> otherwise</li>
     *         </ul>
     * @throws MacroHeaderException if a header is detected but it is not well formated
     */
    public boolean extractMacroFileHead(String macroPath) throws MacroHeaderException {
        boolean result;
        if ((macroPath != null) && !macroPath.trim().isEmpty()) {
            File macroFile = new File(macroPath);
            if (macroFile.isDirectory() || !macroFile.exists()) {
                result = false;
            } else {
                String macro = MacroCommentsRemover.extractMacroWithoutComments(macroFile);
                result = extractHeadFromClearedMacro(macro);
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Analyzes an ImageJ macro to extract its header keys and values. It also stores the macro in memory.
     * 
     * @param macro the macro to analyze. If <code>null</code> or empty, it will have no effect on this
     *            MacroHeaderExtractor. Otherwise, the macro is stored, and its corresponding keys and values (even if
     *            none are found) too.
     * @return a boolean value.
     *         <ul>
     *         <li><code>true</code> if the macro was successfully analyzed, which means if its header was present and
     *         correctly formated</li>
     *         <li><code>false</code> otherwise</li>
     *         </ul>
     * @throws MacroHeaderException if a header is detected but it is not well formated
     */
    public boolean extractMacroHead(String macro) throws MacroHeaderException {
        boolean result;
        if (macro == null) {
            result = false;
        } else {
            macro = MacroCommentsRemover.removeComments(new StringReader(macro));
            result = extractHeadFromClearedMacro(macro);
        }
        return result;
    }

    /**
     * Analyzes an ImageJ macro to extract its header keys and values. It also stores the macro in memory. Here, the
     * macro is supposed to be cleared from comments in header (comments with "/*")
     * 
     * @param macro the macro to analyze. If <code>null</code> or empty, it will have no effect on this
     *            MacroHeaderExtractor. Otherwise, the macro is stored, and its corresponding keys and values (even if
     *            none are found) too.
     * @return a boolean value.
     *         <ul>
     *         <li><code>true</code> if the macro was successfully analyzed, which means if its header was present and
     *         correctly formated</li>
     *         <li><code>false</code> otherwise</li>
     *         </ul>
     * @throws MacroHeaderException if a header is detected but it is not well formated
     */
    protected boolean extractHeadFromClearedMacro(String macro) throws MacroHeaderException {
        boolean result;
        if ((macro == null) || macro.trim().isEmpty()) {
            result = false;
        } else {
            header.clear();
            boolean started = false;
            String[] lines = macro.split(MacroUtils.NEW_LINE);
            for (int i = 0; i < lines.length; i++) {
                String line = lines[i].trim();
                if (!line.isEmpty()) {
                    // empty lines ignored
                    if (line.indexOf(MacroUtils.DOUBLE_SLASH) == 0 && line.length() > 2) {
                        // this is a macro comment
                        // remove 1st "//"
                        line = line.substring(2, line.length());
                        line = line.trim();
                        if (started) {
                            int spliterIndex = line.indexOf(':');
                            if (spliterIndex > 0 && spliterIndex < line.length() - 1) {
                                String key = line.substring(0, spliterIndex).trim();
                                String value = line.substring(spliterIndex + 1, line.length()).trim();
                                if (key.indexOf(' ') > 0 || key.indexOf('/') > 0) {
                                    // bad formated key
                                    throw new MacroHeaderException("bad formated key at following line:\n\"" + lines[i]
                                            + MacroUtils.DOUBLE_QUOT);
                                }
                                header.put(key, value);
                            } else {
                                if ("HEADER_END".equals(line)) {
                                    // end of header --> stop analyzing
                                    break;
                                } else {
                                    // not in header language, but still in
                                    // header --> cancel analyzing and throw
                                    // exception
                                    header.clear();
                                    StringBuilder errorBuffer = new StringBuilder();
                                    errorBuffer.append("Error in macro header. ");
                                    errorBuffer.append("Following line can't be interprated:");
                                    errorBuffer.append("\n\"");
                                    errorBuffer.append(lines[i]);
                                    errorBuffer.append('"');
                                    throw new MacroHeaderException(errorBuffer.toString());
                                }
                            }
                        } else {
                            if ("HEADER_START".equals(line)) {
                                // start of header
                                started = true;
                                continue;
                            } else {
                                // not in header language --> stop analyzing
                                break;
                            }
                        }
                    } else {
                        // not in header language --> stop analyzing
                        break;
                    }
                }
            }
            lastMacro = macro;
            result = !header.isEmpty();
        }
        return result;
    }

    public void fillHeaderMap(Map<String, String> headerMap) {
        if (headerMap != null) {
            headerMap.putAll(header);
        }
    }

    /**
     * Returns a copy of the map
     * 
     * @return an {@link HashMap}
     */
    public Map<String, String> getHeaderCopy() {
        Map<String, String> copy = new HashMap<>();
        fillHeaderMap(copy);
        return copy;
    }

    @Override
    public MacroHeaderExtractor clone() throws CloneNotSupportedException {
        MacroHeaderExtractor extractor = (MacroHeaderExtractor) super.clone();
        if (lastMacro != null) {
            extractor.lastMacro = new String(lastMacro);
        }
        return extractor;
    }

}
