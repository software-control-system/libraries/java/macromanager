package fr.soleil.ij.macro.header;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * A class used by {@link MacroHeaderExtractor} to filter comments from macro
 * header, to extract more easily the keys and values.
 * 
 * @author GIRARDOT
 */
public class MacroCommentsRemover {

    /**
     * Filters the comments of the header of a macro code.
     * 
     * @param macro The macro code
     * @return The same macro code, with no header comment
     */
    public static String removeComments(String macro) {
        String result;
        if (macro == null) {
            result = null;
        } else if (macro.trim().isEmpty()) {
            result = macro;
        } else {
            result = removeComments(new StringReader(macro));
        }
        return result;
    }

    /**
     * Filters the comments of the header of a macro file.
     * 
     * @param macroFile The {@link File} that contains the macro code
     * @return The macro code, with no header comment
     */
    public static String extractMacroWithoutComments(File macroFile) {
        String result;
        if ((macroFile == null) || (macroFile.isDirectory()) || (!macroFile.exists())) {
            result = null;
        } else {
            try {
                result = removeComments(new FileReader(macroFile));
            } catch (FileNotFoundException e) {
                // should never happen due to previous test
                result = null;
            }
        }
        return result;
    }

    /**
     * Filters the comments of the header of a macro handled by a {@link Reader}
     * 
     * @param reader The reader that handles the macro
     * @return The macro code, with no header comment
     */
    protected static String removeComments(Reader reader) {
        StringBuilder resultBuffer = new StringBuilder();
        int c = 0;
        boolean ignore = false;
        boolean wasSlash = false;
        boolean wasStar = false;
        try {
            while (true) {
                c = reader.read();
                if (c == -1) {
                    if (!ignore) {
                        if (wasSlash) {
                            resultBuffer.append('/');
                        }
                        if (wasStar) {
                            resultBuffer.append('*');
                        }
                    }
                    break;
                }
                if ('/' == c) {
                    if (ignore) {
                        if (wasStar) {
                            ignore = false;
                            wasSlash = false;
                            wasStar = false;
                            resultBuffer.append(' ');
                            continue;
                        } else {
                            wasSlash = false;
                            wasStar = false;
                            continue;
                        }
                    } else {
                        if (wasStar) {
                            resultBuffer.append('*');
                        }
                        if (wasSlash) {
                            resultBuffer.append('/');
                        }
                        wasSlash = true;
                        wasStar = false;
                    }
                } else if ('*' == c) {
                    if (ignore) {
                        wasStar = true;
                        wasSlash = false;
                    } else {
                        if (wasSlash) {
                            ignore = true;
                            wasSlash = false;
                            wasStar = false;
                            continue;
                        } else {
                            if (wasStar) {
                                resultBuffer.append('*');
                            }
                            wasStar = true;
                            continue;
                        }
                    }
                } else {
                    if (ignore) {
                        wasSlash = false;
                        wasStar = false;
                        continue;
                    } else {
                        if (wasSlash) {
                            resultBuffer.append('/');
                        }
                        if (wasStar) {
                            resultBuffer.append('*');
                        }
                        wasSlash = false;
                        wasStar = false;
                        resultBuffer.append((char) c);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultBuffer.toString();
    }

}
