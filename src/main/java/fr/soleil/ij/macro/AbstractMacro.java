package fr.soleil.ij.macro;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fr.soleil.ij.macro.element.BooleanImageMacroElement;
import fr.soleil.ij.macro.element.BooleanScalarMacroElement;
import fr.soleil.ij.macro.element.BooleanSpectrumMacroElement;
import fr.soleil.ij.macro.element.DoubleImageMacroElement;
import fr.soleil.ij.macro.element.DoubleScalarMacroElement;
import fr.soleil.ij.macro.element.DoubleSpectrumMacroElement;
import fr.soleil.ij.macro.element.LongImageMacroElement;
import fr.soleil.ij.macro.element.LongScalarMacroElement;
import fr.soleil.ij.macro.element.LongSpectrumMacroElement;
import fr.soleil.ij.macro.element.StringImageMacroElement;
import fr.soleil.ij.macro.element.StringScalarMacroElement;
import fr.soleil.ij.macro.element.StringSpectrumMacroElement;
import fr.soleil.ij.macro.exception.MacroCompatibilityException;
import fr.soleil.ij.macro.exception.MacroHeaderException;
import fr.soleil.ij.macro.header.MacroHeaderExtractor;
import fr.soleil.ij.macro.interactable.MacroInteractable;
import fr.soleil.ij.macro.interactable.MacroInteractableList;
import fr.soleil.ij.macro.interactable.MacroInteractableSingle;
import fr.soleil.ij.macro.util.MacroUtils;

/**
 * Class used to hold the structure to represent an ImageJ macro.
 *
 * @author GIRARDOT
 */
public abstract class AbstractMacro implements Cloneable {

    protected String macro;
    protected String description;
    protected MacroInteractable<?>[] parameters;
    protected MacroInteractable<?>[] results;
    protected MacroHeaderExtractor extractor;
    protected Map<String, String> header;
    protected Map<String, String> publicHeader;

    public AbstractMacro(String macro) {
        super();
        this.macro = macro;
        this.description = null;
        this.extractor = new MacroHeaderExtractor();
        this.header = new HashMap<>();
        this.publicHeader = Collections.unmodifiableMap(header);
        this.parameters = null;
        this.results = null;
    }

    protected static boolean checkIsFile(String macro) {
        boolean file;
        try {
            File tempFile = new File(macro);
            if (tempFile.exists()) {
                file = true;
            } else {
                file = false;
            }
        } catch (Exception e) {
            file = false;
        }
        return file;
    }

    protected void unregisterInteractables() {
        if (parameters != null) {
            for (int i = 0; i < parameters.length; i++) {
                if (parameters[i] != null) {
                    parameters[i].unregister();
                }
            }
        }
        if (results != null) {
            for (int i = 0; i < results.length; i++) {
                if (results[i] != null) {
                    results[i].unregister();
                }
            }
        }
    }

    /**
     * Returns the macro associated with this {@link Macro}
     *
     * @return a {@link String}
     */
    public String getMacro() {
        return macro;
    }

    /**
     * Returns a description of this macro
     *
     * @return a {@link String}
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the macro parameters
     *
     * @return a {@link MacroInteractable} array
     */
    public MacroInteractable<?>[] getParameters() {
        return parameters;
    }

    /**
     * Returns the macro results
     *
     * @return a {@link MacroInteractable} array
     */
    public MacroInteractable<?>[] getResults() {
        return results;
    }

    /**
     * Returns the macro header
     *
     * @return an {@link Map}
     */
    public Map<String, String> getHeader() {
        return publicHeader;
    }

    @Override
    public AbstractMacro clone() {
        try {
            AbstractMacro result = (AbstractMacro) super.clone();
            if (parameters == null) {
                result.parameters = null;
            } else {
                result.parameters = new MacroInteractable<?>[parameters.length];
                for (int i = 0; i < parameters.length; i++) {
                    if (parameters[i] == null) {
                        result.parameters[i] = null;
                    } else {
                        result.parameters[i] = parameters[i].clone();
                    }
                }
            }
            if (results == null) {
                result.results = null;
            } else {
                result.results = new MacroInteractable<?>[results.length];
                for (int i = 0; i < results.length; i++) {
                    if (results[i] == null) {
                        result.results[i] = null;
                    } else {
                        result.results[i] = results[i].clone();
                    }
                }
            }
            if (extractor == null) {
                result.extractor = null;
            } else {
                result.extractor = extractor.clone();
            }
            result.header = new HashMap<>(header);
            result.publicHeader = Collections.unmodifiableMap(result.header);
            return result;
        } catch (CloneNotSupportedException cdse) {
            // should never happen
            return this;
        }
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link LongScalarMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link LongScalarMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link LongScalarMacroElement}
     */
    public static LongScalarMacroElement extractLongScalarMacroElement(String toExtract)
            throws MacroCompatibilityException {
        LongScalarMacroElement element = new LongScalarMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link DoubleScalarMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link DoubleScalarMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link DoubleScalarMacroElement}
     */
    public static DoubleScalarMacroElement extractDoubleScalarMacroElement(String toExtract)
            throws MacroCompatibilityException {
        DoubleScalarMacroElement element = new DoubleScalarMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link BooleanScalarMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link BooleanScalarMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link BooleanScalarMacroElement}
     */
    public static BooleanScalarMacroElement extractBooleanScalarMacroElement(String toExtract)
            throws MacroCompatibilityException {
        BooleanScalarMacroElement element = new BooleanScalarMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link StringScalarMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link StringScalarMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link StringScalarMacroElement}
     */
    public static StringScalarMacroElement extractStringScalarMacroElement(String toExtract)
            throws MacroCompatibilityException {
        StringScalarMacroElement element = new StringScalarMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link LongSpectrumMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link LongSpectrumMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link LongSpectrumMacroElement}
     */
    public static LongSpectrumMacroElement extractLongSpectrumMacroElement(String toExtract)
            throws MacroCompatibilityException {
        LongSpectrumMacroElement element = new LongSpectrumMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link DoubleSpectrumMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link DoubleSpectrumMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link DoubleSpectrumMacroElement}
     */
    public static DoubleSpectrumMacroElement extractDoubleSpectrumMacroElement(String toExtract)
            throws MacroCompatibilityException {
        DoubleSpectrumMacroElement element = new DoubleSpectrumMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link BooleanSpectrumMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link BooleanSpectrumMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link BooleanSpectrumMacroElement}
     */
    public static BooleanSpectrumMacroElement extractBooleanSpectrumMacroElement(String toExtract)
            throws MacroCompatibilityException {
        BooleanSpectrumMacroElement element = new BooleanSpectrumMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link StringSpectrumMacroElement}
     *
     * @param toExtract the {@link String}
     * @return a {@link StringSpectrumMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link StringSpectrumMacroElement}
     */
    public static StringSpectrumMacroElement extractStringSpectrumMacroElement(String toExtract)
            throws MacroCompatibilityException {
        StringSpectrumMacroElement element = new StringSpectrumMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link LongImageMacroElement}
     *
     * @param toExtract the {@link String}
     * @param clean a boolean to know whether the adapter that stored the expected image value should be asked to remove
     *            it, in order to free memory space. <code>TRUE</code> to remove the value in adapter.
     * @return a {@link LongImageMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link LongImageMacroElement}
     */
    public static LongImageMacroElement extractLongImageMacroElement(String toExtract, boolean clean)
            throws MacroCompatibilityException {
        LongImageMacroElement element = new LongImageMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link DoubleImageMacroElement}
     *
     * @param toExtract the {@link String}
     * @param clean a boolean to know whether the adapter that stored the expected image value should be asked to remove
     *            it, in order to free memory space. <code>TRUE</code> to remove the value in adapter.
     * @return a {@link DoubleImageMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link DoubleImageMacroElement}
     */
    public static DoubleImageMacroElement extractDoubleImageMacroElement(String toExtract, boolean clean)
            throws MacroCompatibilityException {
        DoubleImageMacroElement element = new DoubleImageMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link BooleanImageMacroElement}
     *
     * @param toExtract the {@link String}
     * @param clean a boolean to know whether the adapter that stored the expected image value
     *            should be asked to remove it, in order to free memory space. <code>TRUE</code> to remove the
     *            value in adapter.
     * @return a {@link BooleanImageMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link BooleanImageMacroElement}
     */
    public static BooleanImageMacroElement extractBooleanImageMacroElement(String toExtract, boolean clean)
            throws MacroCompatibilityException {
        BooleanImageMacroElement element = new BooleanImageMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    /**
     * Analyzes a {@link String} to extract its corresponding {@link StringImageMacroElement}
     *
     * @param toExtract the {@link String}
     * @param clean a boolean to know whether the adapter that stored the expected image value should be asked to remove
     *            it, in order to free memory space. <code>TRUE</code> to remove the value in adapter.
     * @return a {@link StringImageMacroElement}
     * @throws MacroCompatibilityException If the {@link String} is not well formatted enough to extract a
     *             {@link StringImageMacroElement}
     */
    public static StringImageMacroElement extractStringImageMacroElement(String toExtract, boolean clean)
            throws MacroCompatibilityException {
        StringImageMacroElement element = new StringImageMacroElement();
        element.updateFromStringRepresentation(toExtract);
        return element;
    }

    // Method used to generate an exception corresponding to a problem with
    // cardinality sub-parameter.
    protected static MacroHeaderException generateCardinalityException(int index, String cardinality,
            String kindOfInteractable) {
        StringBuilder errorBuffer = new StringBuilder();
        errorBuffer.append("Header badly formatted: ");
        errorBuffer.append("unknown cardinality for ");
        errorBuffer.append(kindOfInteractable).append(MacroUtils.SPACE);
        errorBuffer.append(index).append(".\n");
        errorBuffer.append("Expected \"1\" or \"n\", found \"");
        errorBuffer.append(cardinality).append(MacroUtils.DOUBLE_QUOT);
        return new MacroHeaderException(errorBuffer.toString());
    }

    // Method used to generate an exception corresponding to a problem with type
    // sub-parameter.
    protected static MacroHeaderException generateTypeException(int index, String type, String kindOfInteractable) {
        StringBuilder errorBuffer = new StringBuilder();
        errorBuffer.append("Header badly formatted: ");
        errorBuffer.append(MacroUtils.DOUBLE_QUOT).append(type).append(MacroUtils.DOUBLE_QUOT);
        errorBuffer.append("is not a valid type for ");
        errorBuffer.append(kindOfInteractable).append(MacroUtils.SPACE).append(index);
        return new MacroHeaderException(errorBuffer.toString());
    }

    // Analyzes a String value to obtain the value to set to a MacroInteractable
    @SuppressWarnings("unchecked")
    protected static void updateInteractable(MacroInteractable<?> interactable, String value, String kindOfInteractable)
            throws MacroCompatibilityException {
        if (interactable == null) {
            throw new MacroCompatibilityException("Can not set a null " + kindOfInteractable);
        } else if (value == null) {
            throw new MacroCompatibilityException("Can not analyze a null String");
        } else {
            value = value.trim();
            try {
                if (interactable instanceof MacroInteractableSingle<?>) {
                    // case MacroInteractableSingle
                    switch (interactable.getExpectedElementType()) {
                        case SCALAR_INT:
                            ((MacroInteractableSingle<LongScalarMacroElement>) interactable)
                                    .setValue(extractLongScalarMacroElement(value));
                            break;
                        case SCALAR_FLOAT:
                            ((MacroInteractableSingle<DoubleScalarMacroElement>) interactable)
                                    .setValue(extractDoubleScalarMacroElement(value));
                            break;
                        case SCALAR_BOOLEAN:
                            ((MacroInteractableSingle<BooleanScalarMacroElement>) interactable)
                                    .setValue(extractBooleanScalarMacroElement(value));
                            break;
                        case SCALAR_STRING:
                            ((MacroInteractableSingle<StringScalarMacroElement>) interactable)
                                    .setValue(extractStringScalarMacroElement(value));
                            break;
                        case SPECTRUM_INT:
                            ((MacroInteractableSingle<LongSpectrumMacroElement>) interactable)
                                    .setValue(extractLongSpectrumMacroElement(value));
                            break;
                        case SPECTRUM_FLOAT:
                            ((MacroInteractableSingle<DoubleSpectrumMacroElement>) interactable)
                                    .setValue(extractDoubleSpectrumMacroElement(value));
                            break;
                        case SPECTRUM_BOOLEAN:
                            ((MacroInteractableSingle<BooleanSpectrumMacroElement>) interactable)
                                    .setValue(extractBooleanSpectrumMacroElement(value));
                            break;
                        case SPECTRUM_STRING:
                            ((MacroInteractableSingle<StringSpectrumMacroElement>) interactable)
                                    .setValue(extractStringSpectrumMacroElement(value));
                            break;
                        case IMAGE_INT:
                            ((MacroInteractableSingle<LongImageMacroElement>) interactable)
                                    .setValue(extractLongImageMacroElement(value, true));
                            break;
                        case IMAGE_FLOAT:
                            ((MacroInteractableSingle<DoubleImageMacroElement>) interactable)
                                    .setValue(extractDoubleImageMacroElement(value, true));
                            break;
                        case IMAGE_BOOLEAN:
                            ((MacroInteractableSingle<BooleanImageMacroElement>) interactable)
                                    .setValue(extractBooleanImageMacroElement(value, true));
                            break;
                        case IMAGE_STRING:
                            ((MacroInteractableSingle<StringImageMacroElement>) interactable)
                                    .setValue(extractStringImageMacroElement(value, true));
                            break;
                    }
                } else {
                    // case MacroInteractableList
                    String[] array = value.split(MacroUtils.NEW_LINE);
                    switch (interactable.getExpectedElementType()) {
                        case SCALAR_INT:
                            ArrayList<LongScalarMacroElement> longScalarList = new ArrayList<LongScalarMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                longScalarList.add(extractLongScalarMacroElement(array[i]));
                            }
                            ((MacroInteractableList<LongScalarMacroElement>) interactable).setValue(longScalarList);
                            break;
                        case SCALAR_FLOAT:
                            ArrayList<DoubleScalarMacroElement> doubleScalarList = new ArrayList<DoubleScalarMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                doubleScalarList.add(extractDoubleScalarMacroElement(array[i]));
                            }
                            ((MacroInteractableList<DoubleScalarMacroElement>) interactable).setValue(doubleScalarList);
                            break;
                        case SCALAR_BOOLEAN:
                            ArrayList<BooleanScalarMacroElement> booleanScalarList = new ArrayList<BooleanScalarMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                booleanScalarList.add(extractBooleanScalarMacroElement(array[i]));
                            }
                            ((MacroInteractableList<BooleanScalarMacroElement>) interactable)
                                    .setValue(booleanScalarList);
                            break;
                        case SCALAR_STRING:
                            ArrayList<StringScalarMacroElement> stringList = new ArrayList<StringScalarMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                stringList.add(extractStringScalarMacroElement(array[i]));
                            }
                            ((MacroInteractableList<StringScalarMacroElement>) interactable).setValue(stringList);
                            break;
                        case SPECTRUM_INT:
                            ArrayList<LongSpectrumMacroElement> longSpectrumList = new ArrayList<LongSpectrumMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                longSpectrumList.add(extractLongSpectrumMacroElement(array[i]));
                            }
                            ((MacroInteractableList<LongSpectrumMacroElement>) interactable).setValue(longSpectrumList);
                            break;
                        case SPECTRUM_FLOAT:
                            ArrayList<DoubleSpectrumMacroElement> doubleSpectrumList = new ArrayList<DoubleSpectrumMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                doubleSpectrumList.add(extractDoubleSpectrumMacroElement(array[i]));
                            }
                            ((MacroInteractableList<DoubleSpectrumMacroElement>) interactable)
                                    .setValue(doubleSpectrumList);
                            break;
                        case SPECTRUM_BOOLEAN:
                            ArrayList<BooleanSpectrumMacroElement> booleanSpectrumList = new ArrayList<BooleanSpectrumMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                booleanSpectrumList.add(extractBooleanSpectrumMacroElement(array[i]));
                            }
                            ((MacroInteractableList<BooleanSpectrumMacroElement>) interactable)
                                    .setValue(booleanSpectrumList);
                            break;
                        case SPECTRUM_STRING:
                            ArrayList<StringSpectrumMacroElement> stringSpectrumList = new ArrayList<StringSpectrumMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                stringSpectrumList.add(extractStringSpectrumMacroElement(array[i]));
                            }
                            ((MacroInteractableList<StringSpectrumMacroElement>) interactable)
                                    .setValue(stringSpectrumList);
                            break;
                        case IMAGE_INT:
                            ArrayList<LongImageMacroElement> longImageList = new ArrayList<LongImageMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                longImageList.add(extractLongImageMacroElement(array[i], true));
                            }
                            ((MacroInteractableList<LongImageMacroElement>) interactable).setValue(longImageList);
                            break;
                        case IMAGE_FLOAT:
                            ArrayList<DoubleImageMacroElement> doubleImageList = new ArrayList<DoubleImageMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                doubleImageList.add(extractDoubleImageMacroElement(array[i], true));
                            }
                            ((MacroInteractableList<DoubleImageMacroElement>) interactable).setValue(doubleImageList);
                            break;
                        case IMAGE_BOOLEAN:
                            ArrayList<BooleanImageMacroElement> booleanImageList = new ArrayList<BooleanImageMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                booleanImageList.add(extractBooleanImageMacroElement(array[i], true));
                            }
                            ((MacroInteractableList<BooleanImageMacroElement>) interactable).setValue(booleanImageList);
                            break;
                        case IMAGE_STRING:
                            ArrayList<StringImageMacroElement> stringImageList = new ArrayList<StringImageMacroElement>();
                            for (int i = 0; i < array.length; i++) {
                                stringImageList.add(extractStringImageMacroElement(array[i], true));
                            }
                            ((MacroInteractableList<StringImageMacroElement>) interactable).setValue(stringImageList);
                            break;
                    } // end if (interactable instanceof
                      // MacroInteractableSingle<?>) ... else
                }
            } catch (Exception e) {
                throw new MacroCompatibilityException(
                        "Incompatible kind of " + kindOfInteractable + ", " + interactable.getDescription(), e);
            }
        }
    }

}
